<?php
  /**
  * Файл Index.php
  *
  *
  * виводить контент матеріалів на WordPress
  *
  * @package WordPress
  * @subpackage ITmoms Web-Design Studio
  * @since ITmoms Site Web-Design Studio 1.0
  */
?>


 
<!DOCTYPE html>

<!--[if IE 7]> <html class="ie ie7" <?php language_attributes(); ?>> <![endif]--> 
<!--[if IE 8]> <html class="ie ie8" <?php language_attributes(); ?>> <![endif]--> 
<!--[if !(IE 7) | !(IE 8) ]><!--> <html <?php language_attributes(); ?>> <!--<![endif]--> 



<head>

  <meta charset="<?php bloginfo( 'charset' ); ?>"/>

  <meta name="viewport" content="width=device-width, initial-scale=1"/>

  <title><?php wp_title( '|', true, 'right' ); ?>Vallo scetches</title>
  
  <link rel="profile" href="http://gmpg.org/xfn/11"/>

  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>"/>

  <link rel='stylesheet' id='main-style' href='<?php echo get_stylesheet_uri(); ?>?ver=1.7' type='text/css' media='all' />

 <!-- <link rel="shortcut icon" type="image/ico" href="http:shop.intech.lviv.ua/public_html/favicon.ico" /> -->
 
    <!-- Compiled and minified bootstrap CSS  -->
    <link rel="stylesheet" href="https://yastatic.net/bootstrap/3.3.6/css/bootstrap.min.css"/>
  
    <!-- Compiled and minified bootstrap JS  -->
	<link rel="stylesheet" href="https://yastatic.net/bootstrap/3.3.6/js/bootstrap.min.js"/>
    
    <!-- Minified Jquery JS -->
	<!--<link rel="stylesheet" href=" https://yastatic.net/jquery/3.1.0/jquery.min.js"/>-->
  <script src='https://www.google.com/recaptcha/api.js'></script>   
 

  <?php wp_head(); ?>

</head>

<body >
 <div class="container">
	<header>
		<nav role ="navigation" class="navbar navbar-mini">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<img height="50" src="http://shop.intech.lviv.ua/wp-content/uploads/2016/11/minilogo.png" width="88"/>
                <a href="#" class="navbar-brand visible-xs">Vallo</a>
			</div>
			<!-- Collection of nav links, forms, and other content for toggling -->
			<div id="navbarCollapse" class="collapse navbar-collapse">
				
				<ul class="nav navbar-nav navbar-right">
 
 <?php if ( is_active_sidebar( 'top_icons' ) ) : ?>

 <?php dynamic_sidebar( 'top_icons' ); ?>

 <?php endif; ?>
 
					<li class="hidden-xs"></li>
					
					<li>
						<form role="search" class="navbar-form visible-xs">
							<div class="form-group input-group input-group-sm">
								<span class="input-group-addon"><span class="glyphicon glyphicon-search"></span></span>
								<input type="text" placeholder="" class="form-control"/>
								<span class="input-group-btn">
								  <button class="btn btn-default" type="submit">Шукати</button>
								</span>
							</div>
						</form>

						<form class="navbar-form radius hidden-xs" role="search">
							<a href="#"><img height="15" src="http://shop.intech.lviv.ua/wp-content/uploads/2016/11/minisearch.png" width="15"/></a>
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Шукати"/>
							</div>
						</form>
					</li>
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#">UA</a>
						<ul class="dropdown-menu">
							<li><a href="#">RU</a></li>
							<li><a href="#">EN</a></li>
							<li><a href="#">PL</a></li>
                            <li><a href="#">DEN</a></li>
						</ul>
					</li>
				</ul>
               
                <div class="nav navbar-nav visible-xs">
					<?php wp_nav_menu( array( 'theme_location' => 'Vallo_pages' ) ); ?>
				</div>
			</div>
		</nav>

		<nav class="navbar navbar-orange hidden-xs">
			    <div class="navbar-header">
					<a class="navbar-brand" href="http://shop.intech.lviv.ua/"><img height="63" src="http://shop.intech.lviv.ua/wp-content/uploads/2016/11/logo.png" width="170"/></a>
				</div>
				
                
                <ul class="nav navbar-nav navbar-right">
					<li><?php wp_nav_menu( array( 'menu' => 'vallo_main' ) ); ?></li>
   					<li><?php wp_nav_menu( array( 'menu' => 'vallo_goods' ) ); ?></li>
					<li><?php wp_nav_menu( array( 'menu' => 'vallo_catalogues' ) ); ?>
                    <?php

  $taxonomy     = 'product_cat';
  $orderby      = 'name';  
  $show_count   = 0;      // 1 for yes, 0 for no
  $pad_counts   = 0;      // 1 for yes, 0 for no
  $hierarchical = 1;      // 1 for yes, 0 for no  
  $title        = '';  
  $empty        = 0;

  $args = array(
         'taxonomy'     => $taxonomy,
         'orderby'      => $orderby,
         'show_count'   => $show_count,
         'pad_counts'   => $pad_counts,
         'hierarchical' => $hierarchical,
         'title_li'     => $title,
         'hide_empty'   => $empty
  );
 $all_categories = get_categories( $args );
 ?>
        
        <ul class="top_menu">
    <?php
 foreach ($all_categories as $cat) {
    if($cat->category_parent == 0 and
    $cat->slug !== 'actions' and
    $cat->slug !== 'favorite'
    ) {
        $category_id = $cat->term_id;       
        echo '<li class="fadeInRight wow">';
        echo '<a href="'. get_term_link($cat->slug, 'product_cat') .'">'. $cat->name .'</a>';

        $args2 = array(
                'taxonomy'     => $taxonomy,
                'child_of'     => 0,
                'parent'       => $category_id,
                'orderby'      => $orderby,
                'show_count'   => $show_count,
                'pad_counts'   => $pad_counts,
                'hierarchical' => $hierarchical,
                'title_li'     => $title,
                'hide_empty'   => $empty
        );
        
        $sub_cats = get_categories( $args2 );
            if($sub_cats) { ?>
            <div class="sub_afterus"></div> 
            <ul class="top_menu sub_categorius_menu">
                <?php
                foreach($sub_cats as $sub_category) {
                    echo  '<li><a href="'. get_term_link($sub_category->slug, 'product_cat') .'">'. $sub_category->name .'</a></li>';
                }
                ?>
                <div class="sub_closerr"></div>  
                
                </ul>
                <?php
            }
    echo '</li>';
    }
}
?></ul>
                    
                    
                    </li>
					<li><?php wp_nav_menu( array( 'menu' => 'vallo_about_us' ) ); ?></li>
					<li><?php wp_nav_menu( array( 'menu' => 'vallo_contacts' ) ); ?></li>
				</ul>
		</nav>

		<ul class="left-tel">
        
  <?php if ( is_active_sidebar( 'phone_numbers' ) ) : ?>

  <?php dynamic_sidebar( 'phone_numbers' ); ?>

  <?php endif; ?>
  
			
			
		</ul>
	</header>