<?php
/**
 * Shop breadcrumb
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/breadcrumb.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 * @see         woocommerce_breadcrumb()
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}



if ( ! empty( $breadcrumb ) ) {    

	echo $wrap_before;

	foreach ( $breadcrumb as $key => $crumb ) {

		echo $before;

		if ( ! empty( $crumb[1] ) 
            && sizeof( $breadcrumb ) !== $key + 1
            && !(sizeof( $breadcrumb ) == $key + 2 and is_product() )
            ) {
			echo '<a href="' . esc_url( $crumb[1] ) . '">' . esc_html( $crumb[0] ) . '</a>';
		} 
        elseif(is_product()){
            /* 
            ## for cose if we want detect parent cat of product category
                $terms_post = get_the_terms( $post->cat_ID , 'product_cat' );
                foreach ($terms_post as $term_cat) { 
                if ($term_cat->parent == 0){
                    echo esc_html( $crumb[0] );
                }
                else {echo '<a href="' . esc_url( $crumb[1] ) . '">' . esc_html( $crumb[0] ) . '</a>';}
            }
            */
                                 
            echo esc_html( $crumb[0] );
            #print($cat_id);
        }
        else {
			echo esc_html( $crumb[0] );
		}

		echo $after;

		if ( sizeof( $breadcrumb ) !== $key + 1 ) {
			echo $delimiter;
		}
        

	}

	echo $wrap_after ;
#. ' loock to: /Vallo/woocommerce/global'
}
