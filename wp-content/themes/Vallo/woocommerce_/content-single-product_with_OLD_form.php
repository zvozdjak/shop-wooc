<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/** echo the_ID() . "<br /><br />";
**/

global $product;

?>
<div class="row">
<hr />
<article class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
<!-- full publication -->
<div class="row">						
<?php
    if($product->get_gallery_attachment_ids()){
        ?>
        <!-- Gallery -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">	
        <?php
        $attachment_ids = $product->get_gallery_attachment_ids();
        foreach( $attachment_ids as $attachment_id ) 
        {
          echo '
          <a href="'.$image_link = wp_get_attachment_url( $attachment_id ).'" class="zoom first" title="" data-rel="prettyPhoto[product-gallery]">
          <img src="'.$image_link = wp_get_attachment_url( $attachment_id ).'" alt="" class="pull-left thumbnail img-responsive">
          </a>';
        }
        ?>
        </div>
        <!-- END of Gallery -->
<?php
}
#END of Gallery
?>
                            
<div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
								<h1 class="text-left"><?php	echo $product->get_title();?> </h1> 
									<div class="fullstory">
										<strong>Основні технічні характеристики:</strong>
										<?php wc_get_template( 'single-product/short-description.php' ); ?>
										<p>&nbsp;</p>
										<strong class="prodincat">Можливі розміри комірок:</strong>
										<?php
                                            	isa_woocommerce_all_pa();
                                    ?></div>

									
                                    <div class="clr"></div>
                                    <?php the_content(); ?>
                                    <div class="clr"></div>
<?php
/**
 * Related products
 */ 


	global $product, $woocommerce_loop;

if ( $upsells = $product->get_upsells() ) {
	$args = array(
	'post_type'           => 'product',
	'ignore_sticky_posts' => 1,
	'no_found_rows'       => 1,
	'posts_per_page'      => $posts_per_page,
	'orderby'             => $orderby,
	'post__in'            => $upsells,
	'post__not_in'        => array( $product->id ),
	'meta_query'          => WC()->query->get_meta_query()
);

$rel_productus                    = new WP_Query( $args );
/*
echo '<pre>';
print_r($rel_productus);
echo '</pre>';
*/
$woocommerce_loop['name']    = 'up-sells';
$woocommerce_loop['columns'] = apply_filters( 'woocommerce_up_sells_columns', $columns );

if ( $rel_productus->have_posts() ) : ?>

<div class="realated-news">
		<?php woocommerce_product_loop_start(); ?>

			<?php while ( $rel_productus->have_posts() ) : $rel_productus->the_post(); ?>
            <strong class="prodincat"><a href="<?php the_permalink(); ?>">
				<?php   
                the_title();
                ?>
            &nbsp;<span class="text-right">&gt;&gt;</span></a></strong>
			<?php endwhile; // end of the loop. ?>

		<?php woocommerce_product_loop_end(); ?>
</div>
<?php endif;
} 
###
# END of related products
###


    

wp_reset_postdata();
    
    
    
?>
										
							</div>
						</div><!-- END of full publication -->
					</article>
                    <?php get_sidebar( 'top_icons' ); ?>
                    </div>

<?php do_action( 'woocommerce_after_single_product' ); ?>
