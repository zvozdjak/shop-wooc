<?php
/**
 * The Sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
<aside class="col-lg-3 col-md-4 col-sm-12 col-xs-12">

<h3><?php echo __('Feedback') ?></h3>
<hr />
<?php  

if (qtranxf_getLanguage() == 'en') {
echo do_shortcode('[contact-form-7 id="1530" title="Vallo_contact_form" html_class="" ]');
} else if (qtranxf_getLanguage() == 'ua') {
echo do_shortcode('[contact-form-7 id="1294" title="Vallo_contact_form" html_class="" ]');
} else if (qtranxf_getLanguage() == 'pl') {
echo do_shortcode('[contact-form-7 id="1528" title="Vallo_contact_form" html_class="" ]');
} else if (qtranxf_getLanguage() == 'ru') {
echo do_shortcode('[contact-form-7 id="1527" title="Vallo_contact_form" html_class="" ]');
} else if (qtranxf_getLanguage() == 'dk') {
echo do_shortcode('[contact-form-7 id="1529" title="Vallo_contact_form" html_class="" ]');
} else {
echo do_shortcode('[contact-form-7 id="1530" title="Vallo_contact_form" html_class="" ]');
} 
?>

</aside>