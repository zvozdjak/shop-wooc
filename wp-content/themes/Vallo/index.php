<?php
  /**
  * Файл Index.php
  *
  *
  * виводить контент матеріалів на WordPress
  *
  * @package WordPress
  * @subpackage ITmoms WebStudio
  * @since ITmoms Site WebStudio 1.0
  */
?>
  
 <?php get_header (); ?>

	<div class="main"> 
		
		<section>
          <div class="row">
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
              <div id="myCarousel" class="carousel slide">
				<?php echo do_shortcode("[huge_it_slider id='1']"); ?>
		       </div>
             </div> 
           </div>      
				
              <div class="row">

      
<article class="banner_cont">                    


       
        <?php 
                    
  $taxonomy     = 'product_cat';
  $orderby      = 'name';  
  $show_count   = 0;      // 1 for yes, 0 for no
  $pad_counts   = 0;      // 1 for yes, 0 for no
  $hierarchical = 1;      // 1 for yes, 0 for no  
  $title        = '';  
  $empty        = 0;

  $args = array(
         'taxonomy'     => $taxonomy,
         'orderby'      => $orderby,
         'show_count'   => $show_count,
         'pad_counts'   => $pad_counts,
         'hierarchical' => $hierarchical,
         'title_li'     => $title,
         'hide_empty'   => $empty
  );
 $all_categories = get_categories( $args );


# start to count
$cat_counter = 0;
 foreach ($all_categories as $cat) {
    
    if($cat->category_parent == 0) {
        
        
        $category_id = $cat->term_id;   
        $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
	    #$image = wp_get_attachment_url( $thumbnail_id );
      $image = wp_get_attachment_image( $thumbnail_id, 'medium', false );

        ?>
<div class=" banner_cat_column ">
    
<?php
if ($cat_counter == 1 ){ 
echo '<p>' . qtranxf_useCurrentLanguageIfNotFoundUseDefaultLanguage($cat->description) . '</p>';

echo  '<a href="' . get_term_link($cat->slug, 'product_cat') . '"
    class="banner_title_box" title="' . $cat->name . '">'. '<h4 >' . $cat->name . ' </h4>' . $image . ' </a>';


} else{
  echo  '<a href="' . get_term_link($cat->slug, 'product_cat') . '"
    class="banner_title_box" title="' . $cat->name . '">'. '<h4 >' . $cat->name . ' </h4>' . $image . ' </a>';

echo '<p>' . qtranxf_useCurrentLanguageIfNotFoundUseDefaultLanguage($cat->description) . '</p>';
}

 

 ?>

</div>

							<?php 
                                #echo $cat->description;
                             
                            
$cat_counter ++;
    
    }       
 }
 # end of foreach
?>

			
            
 </article>       
 <?php 

# end of login user
?>     

</div> 
               
                
</section>        	       
                    
	</div>					
			


<?php get_footer (); ?>

</div>	
		