<?php
  /**
  * Файл Index.php
  *
  *
  * виводить контент матеріалів на WordPress
  *
  * @package WordPress
  * @subpackage ITmoms Web-Design Studio
  * @since ITmoms Site Web-Design Studio 1.0
  */
?>


<footer class="footer">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 col-lg-push-3 vcenter">
                <table>
					<tbody>
					<tr>            	                                                            
                        <?php if ( is_active_sidebar( 'image' ) ) : ?>
                        <?php dynamic_sidebar( 'image' ); ?>
                        <?php endif; ?>              						
                                     
                        <?php if ( is_active_sidebar( 'location' ) ) : ?>
                        <?php dynamic_sidebar( 'location' ); ?>
                        <?php endif; ?>
					</tr>        
                	<tr>						
                        <?php if ( is_active_sidebar( 'imagetel' ) ) : ?>
                        <?php dynamic_sidebar( 'imagetel' ); ?>
                        <?php endif; ?>                        
												                        
                        <?php if ( is_active_sidebar( 'contacts' ) ) : ?>
                        <?php dynamic_sidebar( 'contacts' ); ?>
                        <?php endif; ?>                                                
					</tr>
					</tbody>
				</table>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 col-lg-pull-4 vcenter">
				<img src="http://shop.intech.lviv.ua/wp-content/uploads/2017/05/ValloPSlogo_small.png" />
				    
                    <p>   
                        <?php if ( is_active_sidebar( 'rights' ) ) : ?>
                        <?php dynamic_sidebar( 'rights' ); ?>
                        <?php endif; ?>   
                       
                    </p> 
                 
              <span class="zocial"> 	                 
                <?php if ( is_active_sidebar( 'social_icons' ) ) : ?>
                <?php dynamic_sidebar( 'social_icons' ); ?>
                <?php endif; ?>	
              </span> 
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3  vcenter">			
                <?php if ( is_active_sidebar( 'site' ) ) : ?>
                <?php dynamic_sidebar( 'site' ); ?>
                <?php endif; ?>
                
            </div>
            
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-2">			
                <?php if ( is_active_sidebar( 'mail' ) ) : ?>
                <?php dynamic_sidebar( 'mail' ); ?>
                <?php endif; ?>
                
            </div>

		</footer>
	</div>
 </div>

 
<?php wp_footer(); ?>

<script type="text/javascript">
        
// titles same height
    function set_ingr_height(){
    
    var numbers_of_cols = 3; 
    //alert(numbers_of_cols);
    im = 0;
    colonus = 1;
    height_set = 0;
    var i_ing_line = 0;
     $('.row.mainpage h1, .row.mainpage h3').each(function(i,elem) {
     $(this).wrapInner("<span></span>");
        //alert ('good connection');
        if (im < numbers_of_cols){
            im = +im + 1 ;  
            var column_set = "max_height"+colonus ; 
            
            height_set_new = $(this).height();
            if(height_set < height_set_new){height_set = height_set_new;}            
            
                                      
        }else{
            colonus = +colonus + 1 ;
            im = 0;
            im = +im + 1 ;  
            var column_set = "max_height"+colonus ;                         
            height_set = 0;
            height_set_new = $(this).height();
            if(height_set < height_set_new){height_set = height_set_new;}
            //$(this).find(".roll_ingredients").css({height: height_set+"px", border: "1px dashed #ccc"});                

        }
        $(this).attr("alt", column_set);
        $('[alt="'+column_set+'"]').css({height: height_set+"px"}); 
        //$(this).prepend(height_set);
        
     });
    
    }
    // END of titles same height

jQuery( window ).load(function() {          
                jQuery('.sub_afterus').click(function(a){             
                //alert('good connection');               
                jQuery('.menu-item-1299').toggleClass('sub_active');
                jQuery(this).toggleClass('sub_active');
                return false;
            });
        jQuery('.menu-item-1299 > a').on('click', function(e) {
        e.preventDefault();
        });
});


      
        
</script> 
</body>
</html>
