<?php
  /**
  * Файл Index.php
  *
  *
  * виводить контент матеріалів на WordPress
  *
  * @package WordPress
  * @subpackage ITMoms WebStudio
  * @since ITMoms WebStudio Site 1.0
  */
?>

<!DOCTYPE html>

<!--[if IE 7]> <html class="ie ie7" <?php language_attributes(); ?>> <![endif]--> 
<!--[if IE 8]> <html class="ie ie8" <?php language_attributes(); ?>> <![endif]--> 
<!--[if !(IE 7) | !(IE 8) ]><!--> <html <?php language_attributes(); ?>> <!--<![endif]--> 



<head>

  <meta charset="<?php bloginfo( 'charset' ); ?>"/>

  <meta name="viewport" content="width=device-width, initial-scale=1"/>

  <title><?php wp_title( '|', true, 'right' ); ?>Vallo scetches</title>
  
  <link rel="profile" href="http://gmpg.org/xfn/11"/>

  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>"/>

  <link rel='stylesheet' id='main-style' href='<?php echo get_stylesheet_uri(); ?>?ver=1.10.45' type='text/css' media='all' />
 
    <!-- Compiled and minified bootstrap CSS  -->
	<link rel="stylesheet" href="https://yastatic.net/bootstrap/3.3.6/css/bootstrap.min.css">
	
	<!-- Minified Jquery JS -->
	<script src="https://yastatic.net/jquery/2.2.3/jquery.min.js"></script>
	
    <!-- Compiled and minified bootstrap JS  -->
	<script src="https://yastatic.net/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    
  <script src='https://www.google.com/recaptcha/api.js'></script>   
 
 
  <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
 
 <link rel='stylesheet' id='main-style' href='/wp-content/themes/Vallo/senq.css?ver=1.5' type='text/css' media='all' />

  <?php wp_head(); ?>

</head>

<body >
 <div class="container">
	<header>
		<nav role ="navigation" class="navbar navbar-mini">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbarCollapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span> 
					<span class="icon-bar"></span>
				</button>
				
                <a href="/" class="navbar-brand visible-xs"><img height="50" src="/wp-content/uploads/2017/05/ValloPSlogo_mob.png" /></a>
			</div>        
			<!-- Collection of nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="navbarCollapse">
				<ul class="nav navbar-nav visible-xs">
					<li class="active">
                    <div class="sub_afterus"></div> 
                    <?php wp_nav_menu( array( 'menu' => 'top_static_pages', 'menu_class' => 'nav navbar-nav visible-xs' ) ); ?>
                    </li>
                </ul>
                
                <ul class="nav navbar-nav navbar-right">
                    <li>
                       <ul class="left-tel">
                           <?php if ( is_active_sidebar( 'top_icons' ) ) : ?>
                           <?php dynamic_sidebar( 'top_icons' ); ?>
                           <?php endif; ?>
                       </ul>
                    </li>
                    <li class="langs">
                       <?php if ( is_active_sidebar( 'top_langs' ) ) : ?>
                        <?php dynamic_sidebar( 'top_langs' ); ?>                    
                        <?php endif; ?>
                    </li>	
				</ul>
                       
                <!--<div class="nav navbar-nav visible-xs">
					<?php #wp_nav_menu( array( 'theme_location' => 'Vallo_pages' ) ); ?>
				</div>-->
			</div>
   </nav>

		<nav class="navbar navbar-orange hidden-xs">
			    <div class="navbar-header">
					<a class="navbar-brand" href="/"><img height="63" src="/wp-content/uploads/2017/05/ValloPSlogo_main.png" width="170"/></a>
				</div>
               
				<?php wp_nav_menu( array( 'menu' => 'top_static_pages', 'menu_class' => 'nav navbar-nav navbar-right' ) ); ?>                
            
		</nav>
        
                
       
	</header>
    
    <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>