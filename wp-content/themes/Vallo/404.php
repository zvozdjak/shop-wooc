<?php

/**
 * @author ITMoms WebStudio
 * @copyright 2016
 */

?>
<?php get_header(); ?>

    <div class="main">

		<section>
			<div class="row p404">
				<div class="col-lg-6 col-md-6 col-sm-6 hidden-xs vcenter">
						<img src="/wp-content/uploads/2016/12/vallo_404.png" class="img-responsive"/>
				</div><!--
			--><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 vcenter">
					<h2><?php
			printf(
				/* translators: %s: a link to the embedded site */
				__( 'It looks like nothing was found at this location. Maybe try visiting %s directly?' ),
				'<strong><a href="' . esc_url( home_url() ) . '">' . esc_html( get_bloginfo( 'name' ) ) . '</a></strong>'
			);
			?></h2>
					<h1>404</h1>
			
				</div>
                
			</div>
            
            <div class="clearfix visible-md-block"></div>
                       
              
		</section>

	</div>

<?php get_footer (); ?>
