<?php

/**
 ** виводить контент матеріалів на WordPress
  *
  * @package WordPress
  * @subpackage ITmoms Web-Design Studio
  * @since ITmoms Site Web-Design Studio 1.0
 */
?>

 <?php get_header(); ?>
 <div class="main"> 
       <h1><?php echo __('Search Results for &#8220;%s&#8221;') ?>: "<?php echo $_GET['s'];?>"</h1>
       <ol class="searchresults">
            <?php if(have_posts()):?><?php while(have_posts()):the_post();?>
                 
           	<li>
                <div class="post" id="post-<?php the_ID();?>">
                
                <h4><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></h4>
                
                </div>
            </li>
            <?php endwhile;?>   
       </ol>
            <?php else: ?>
             <h1>За Вашим запитом нічого не знайдено.</h1>
            <?php endif; ?>
     </div>
          
            
 <?php get_footer(); ?>

