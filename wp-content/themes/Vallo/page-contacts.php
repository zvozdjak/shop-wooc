<?php
/*
Template Name: Contacts
*/

add_action( 'topsidebar_77', 'form_displayer' );// sidebar

get_header(); ?>


	<div class="main">

		<section>
				<div class="row">
					<hr />
					<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
						<h4><?php echo __('Offices') ?></h4>
							<table id="location">
								<tr>
									<?php if ( is_active_sidebar( 'clocation' ) ) : ?>
                                    <?php dynamic_sidebar( 'clocation' ); ?>
                                    <?php endif; ?>
								</tr>
								
								<tr>
									<td>
										<a href="#myModal" data-toggle="modal">
										<a href="http://shop.intech.lviv.ua/map/"><img src="http://shop.intech.lviv.ua/wp-content/uploads/2016/11/inmap.png"/></a>	
                                            
											<span><?php echo __("Show on the map") ?></span>
										</a>
									</td>
								</tr>
							</table>


					</div>
					<div class="col-lg-offset-1 col-lg-5 col-md-offset-1 col-md-5 col-sm-6 col-xs-12">
						<h4 class="text-left"><?php echo __('Contact persons') ?></h4>
							<table id="faces">
								<tbody>
								
                                    <?php if ( is_active_sidebar( 'workers' ) ) : ?>
                                    <?php dynamic_sidebar( 'workers' ); ?>
                                    <?php endif; ?>
								
								</tbody>
							</table>
					</div>
					<div class="clearfix visible-md-block"></div>

					<?php get_sidebar( 'top_icons' ); ?>
                    
                </div>    
		</section>
	</div>



<?php get_footer(); ?>
