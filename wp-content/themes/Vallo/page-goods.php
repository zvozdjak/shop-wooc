<?php
/*
Template Name: Goods
*/



get_header(); ?>

	<div class="main">

		<section>
				<div class="row">
					<hr />
<article class="col-lg-9 col-md-8 col-sm-12 col-xs-12">                    
<?php 
while (have_posts()) : the_post(); ?>
        

            
               <?php the_content('Continue reading &raquo;'); ?>
                 

        <?php endwhile; 
        
                    
  $taxonomy     = 'product_cat';
  $orderby      = 'name';  
  $show_count   = 0;      // 1 for yes, 0 for no
  $pad_counts   = 0;      // 1 for yes, 0 for no
  $hierarchical = 1;      // 1 for yes, 0 for no  
  $title        = '';  
  $empty        = 0;

  $args = array(
         'taxonomy'     => $taxonomy,
         'orderby'      => $orderby,
         'show_count'   => $show_count,
         'pad_counts'   => $pad_counts,
         'hierarchical' => $hierarchical,
         'title_li'     => $title,
         'hide_empty'   => $empty
  );
 $all_categories = get_categories( $args );
 foreach ($all_categories as $cat) {
    
    if($cat->category_parent == 0) {
        
        
        $category_id = $cat->term_id;   
        $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
	    $image = wp_get_attachment_url( $thumbnail_id );
	    
        
            
        #echo '<br /><a href="'. get_term_link($cat->slug, 'product_cat') .'">'. $cat->name .'</a>';
        ?>
    <div class="row">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
    <a href="<?php	echo  get_term_link($cat->slug, 'product_cat'); ?>" title="<?php echo $cat->name; ?>"><?php
		    echo '<img src="' . $image . '" alt="" class="pull-left thumbnail img-responsive" />';
?></a>
							
							</div>
                            
     <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
								<h4 class="text-left">
                                <a href="<?php	echo  get_term_link($cat->slug, 'product_cat'); ?>" title="<?php echo $cat->name; ?>"><?php echo $cat->name; ?>
                                </a>
                                </h4>
								<div><?php 
                                echo qtranxf_useCurrentLanguageIfNotFoundUseDefaultLanguage($cat->description);
                                ?></div>
								<div class="text-right"><a href="<?php	echo  get_term_link($cat->slug, 'product_cat'); ?>" title="<?php echo $cat->name; ?>"><?php echo __('Read more...'); ?></a></div>
							</div>   
     </div>
                            
<?php
        /*
        $args2 = array(
                'taxonomy'     => $taxonomy,
                'child_of'     => 0,
                'parent'       => $category_id,
                'orderby'      => $orderby,
                'show_count'   => $show_count,
                'pad_counts'   => $pad_counts,
                'hierarchical' => $hierarchical,
                'title_li'     => $title,
                'hide_empty'   => $empty
        );
        $sub_cats = get_categories( $args2 );
        if($sub_cats) {
            foreach($sub_cats as $sub_category) {
                echo  $sub_category->name ;
            }   
        }
        */
    }       
}
?>

			
            
            </article>
<?php get_sidebar( 'top_icons' ); ?>
           
            	</div>
		</section>



	</div>
	


<?php get_footer(); ?>
