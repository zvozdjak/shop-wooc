<?php


class Workers_Widget extends WP_Widget {

    function __construct() {
             parent::__construct('workers', // Base ID
			 esc_html__( 'Workers', 'text_domain' ), // Name
			 array( 'description' => esc_html__( 'індивідуальний віджет для виводу контактів працівників' ), ) // Args
		     );
	}

	
	/**
	 * Back-end widget form
	 * @see WP_Widget::form()
	 * @param array $instance Previously saved values from database.
	 */
     
	public function form( $instance ) {
		$image = ! empty( $instance['image'] ) ? $instance['image'] : esc_html__( 'New image', 'text_domain' );
        $name = ! empty( $instance['name'] ) ? $instance['name'] : esc_html__( 'Name', 'text_domain' );
        $country = ! empty( $instance['country'] ) ? $instance['country'] : esc_html__( 'Country', 'text_domain' );
        $email = ! empty( $instance['email'] ) ? $instance['email'] : esc_html__( 'Email', 'text_domain' );      
		$lang = ! empty( $instance['lang'] ) ? $instance['lang'] : esc_html__( 'Languages', 'text_domain' );
        $number = ! empty( $instance['number'] ) ? $instance['number'] : esc_html__( 'Phone number', 'text_domain' );
        $url = ! empty( $instance['url'] ) ? $instance['url'] : esc_html__( 'Phone url', 'text_domain' );
        $fax = ! empty( $instance['fax'] ) ? $instance['fax'] : esc_html__( 'Second phone number', 'text_domain' );
        $furl = ! empty( $instance['furl'] ) ? $instance['furl'] : esc_html__( 'Second phone url', 'text_domain' );
        ?>
  <style>.gallery{
            display: none;
        }</style>
        <p>
           <label for="<?php echo esc_attr( $this->get_field_id( 'image' ) ); ?>">
           <?php esc_attr_e( 'image:', 'text_domain' ); ?></label> 
		   <input class="widefat src_of_image" id="<?php echo esc_attr( $this->get_field_id( 'image' ) ); ?>" 
           name="<?php echo esc_attr( $this->get_field_name( 'image' ) ); ?>" type="text" 
           value="<?php echo esc_attr( $image ); ?>"/>
            <label for="<?php echo esc_attr( $this->get_field_id( 'buttonus' ) ); ?>"><div class="buttonus"><?php esc_attr_e( 'Галерея', 'text_domain' ); ?> </div></label>
           <p>
           <?php 
           $gallery_shortcode = '[gallery id="' . intval( $post->post_parent ) . '" order="DESC" orderby="ID" link="none"]';
           print apply_filters( 'the_content', $gallery_shortcode ); ?>
           </p>
           
        </p> 
        <p>
           <label for="<?php echo esc_attr( $this->get_field_id( 'name' ) ); ?>">
           <?php esc_attr_e( 'name', 'text_domain' ); ?></label> 
	       <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'name' ) ); ?>" 
           name="<?php echo esc_attr( $this->get_field_name( 'name' ) ); ?>" type="text" 
           value="<?php echo esc_attr( $name ); ?>"/>
		</p>
        <p>
           <label for="<?php echo esc_attr( $this->get_field_id( 'country' ) ); ?>">
           <?php esc_attr_e( 'country', 'text_domain' ); ?></label> 
	       <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'country' ) ); ?>" 
           name="<?php echo esc_attr( $this->get_field_name( 'country' ) ); ?>" type="text" 
           value="<?php echo esc_attr( $country ); ?>"/>
		</p>
        <p>
           <label for="<?php echo esc_attr( $this->get_field_id( 'email' ) ); ?>">
           <?php esc_attr_e( 'email', 'text_domain' ); ?></label> 
	       <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'email' ) ); ?>" 
           name="<?php echo esc_attr( $this->get_field_name( 'email' ) ); ?>" type="text" 
           value="<?php echo esc_attr( $email ); ?>"/>
		</p>    
        <p>
           <label for="<?php echo esc_attr( $this->get_field_id( 'lang' ) ); ?>">
           <?php esc_attr_e( 'lang', 'text_domain' ); ?></label> 
	       <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'lang' ) ); ?>" 
           name="<?php echo esc_attr( $this->get_field_name( 'lang' ) ); ?>" type="text" 
           value="<?php echo esc_attr( $lang ); ?>"/>
		</p>      
        <p>
		   <label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>">
           <?php esc_attr_e( 'number:', 'text_domain' ); ?></label> 
		   <input class="widefat src_of_number" id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" 
           name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" 
           value="<?php echo esc_attr( $number ); ?>"/>
           <p>
		       <label for="<?php echo esc_attr( $this->get_field_id( 'url' ) ); ?>">
               <?php esc_attr_e( 'url:', 'text_domain' ); ?></label> 
		       <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'url' ) ); ?>" 
               name="<?php echo esc_attr( $this->get_field_name( 'url' ) ); ?>" type="text" 
               value="<?php echo esc_attr( $url ); ?>"/>
		   </p>
        </p>
        
        <p>
		   <label for="<?php echo esc_attr( $this->get_field_id( 'fax' ) ); ?>">
           <?php esc_attr_e( 'fax:', 'text_domain' ); ?></label> 
		   <input class="widefat src_of_fax" id="<?php echo esc_attr( $this->get_field_id( 'fax' ) ); ?>" 
           name="<?php echo esc_attr( $this->get_field_name( 'fax' ) ); ?>" type="text" 
           value="<?php echo esc_attr( $fax ); ?>"/>
           <p>
		       <label for="<?php echo esc_attr( $this->get_field_id( 'furl' ) ); ?>">
               <?php esc_attr_e( 'furl:', 'text_domain' ); ?></label> 
		       <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'furl' ) ); ?>" 
               name="<?php echo esc_attr( $this->get_field_name( 'furl' ) ); ?>" type="text" 
               value="<?php echo esc_attr( $furl ); ?>"/>
		   </p>
        </p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 * @see WP_Widget::update()
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['image'] = ( ! empty( $new_instance['image'] ) ) ? strip_tags( $new_instance['image'] ) : '';
        $instance['name'] = ( ! empty( $new_instance['name'] ) ) ? strip_tags( $new_instance['name'] ) : '';
        $instance['country'] = ( ! empty( $new_instance['country'] ) ) ? strip_tags( $new_instance['country'] ) : '';
        $instance['email'] = ( ! empty( $new_instance['email'] ) ) ? strip_tags( $new_instance['email'] ) : '';
        $instance['lang'] = ( ! empty( $new_instance['lang'] ) ) ? strip_tags( $new_instance['lang'] ) : '';    
		$instance['number'] = ( ! empty( $new_instance['number'] ) ) ? strip_tags( $new_instance['number'] ) : '';
        $instance['url'] = ( ! empty( $new_instance['url'] ) ) ? strip_tags( $new_instance['url'] ) : '';
        $instance['fax'] = ( ! empty( $new_instance['fax'] ) ) ? strip_tags( $new_instance['fax'] ) : '';
        $instance['furl'] = ( ! empty( $new_instance['furl'] ) ) ? strip_tags( $new_instance['furl'] ) : '';
        return $instance;
	}
    
    /**
	 * Front-end display of widget.
	 * @see WP_Widget::widget()
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
        
		if ( ! empty( $instance['image'] ) ) {
			echo ' <tr><td><img src="'.apply_filters( 'widget_title', $instance['image']).'"/></td>
                   <td>'.apply_filters( 'widget_title', $instance['name']).' <br />
                   '.apply_filters( 'widget_title', $instance['country']).' <br /> 
                   Email: <a href="mailto:'.apply_filters( 'widget_title', $instance['email']).'" >' .apply_filters( 'widget_title', $instance['email']). '</a><br />
                    '.__("languages") . ': '.apply_filters( 'widget_title', $instance['lang']).' <br />
                   <a href="tel: '.apply_filters( 'widget_title', $instance['url']).'" >
                   '.apply_filters( 'widget_title', $instance['number']). '</a><br /> 
                   <a href="tel: '.apply_filters( 'widget_title', $instance['furl']).'" >
                   ' .apply_filters( 'widget_title', $instance['fax']). '</a><br /></td></tr> ' ;
		}
		#echo esc_html__( 'Hello, World!', 'text_domain' );
		
		
		
		echo $args['after_widget'];
	}

} // class Workers_Widget

?>