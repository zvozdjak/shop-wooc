<?php

/**
 * @author ITMoms WebStudio
 * @copyright 2017
 */

class Sites_Widget extends WP_Widget {

    function __construct() {
             parent::__construct('site', // Base ID
			 esc_html__( 'Site', 'text_domain' ), // Name
			 array( 'description' => esc_html__( 'індивідуальний віджет для лінків на інші сайти' ), ) // Args
		     );
	}

	
	/**
	 * Back-end widget form
	 * @see WP_Widget::form()
	 * @param array $instance Previously saved values from database.
	 */
     
	public function form( $instance ) {
		$text = ! empty( $instance['text'] ) ? $instance['text'] : esc_html__( 'text', 'text_domain' );
        $site = ! empty( $instance['site'] ) ? $instance['site'] : esc_html__( 'site', 'text_domain' );
		$site1 = ! empty( $instance['site1'] ) ? $instance['site1'] : esc_html__( 'site1', 'text_domain' );
        ?>
             
        <p>
		   <label for="<?php echo esc_attr( $this->get_field_id( 'text' ) ); ?>">
           <?php esc_attr_e( 'text:', 'text_domain' ); ?></label> 
		   <input class="widefat src_of_text" id="<?php echo esc_attr( $this->get_field_id( 'text' ) ); ?>" 
           name="<?php echo esc_attr( $this->get_field_name( 'text' ) ); ?>" type="text" 
           value="<?php echo esc_attr( $text ); ?>"/>
        </p>
        
        <p>
		   <label for="<?php echo esc_attr( $this->get_field_id( 'site' ) ); ?>">
           <?php esc_attr_e( 'site:', 'text_domain' ); ?></label> 
		   <input class="widefat src_of_site" id="<?php echo esc_attr( $this->get_field_id( 'site' ) ); ?>" 
           name="<?php echo esc_attr( $this->get_field_name( 'site' ) ); ?>" type="text" 
           value="<?php echo esc_attr( $site ); ?>"/>
        </p>
            
        <p>
		   <label for="<?php echo esc_attr( $this->get_field_id( 'site1' ) ); ?>">
           <?php esc_attr_e( 'site1:', 'text_domain' ); ?></label> 
		   <input class="widefat src_of_site1" id="<?php echo esc_attr( $this->get_field_id( 'site1' ) ); ?>" 
           name="<?php echo esc_attr( $this->get_field_name( 'site1' ) ); ?>" type="text" 
           value="<?php echo esc_attr( $site1 ); ?>"/>
        </p> 
        
            
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 * @see WP_Widget::update()
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['text'] = ( ! empty( $new_instance['text'] ) ) ? strip_tags( $new_instance['text'] ) : '';
        $instance['site'] = ( ! empty( $new_instance['site'] ) ) ? strip_tags( $new_instance['site'] ) : '';
		$instance['site1'] = ( ! empty( $new_instance['site1'] ) ) ? strip_tags( $new_instance['site1'] ) : '';
        return $instance;
	}
    
    /**
	 * Front-end display of widget.
	 * @see WP_Widget::widget()
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
        
		if ( ! empty( $instance['text'] ) ) {
			echo ' <strong>'.apply_filters( 'widget_title', $instance['text']).'</strong></br>
            <a href="http://'.apply_filters( 'widget_title', $instance['site']).'" target="_blank">
            ' .apply_filters( 'widget_title', $instance['site']). '</a> </br>
            <a href="http://'.apply_filters( 'widget_title', $instance['site1']).'" target="_blank">
            ' .apply_filters( 'widget_title', $instance['site1']). '</a>
            ' ;
		}
		
		echo $args['after_widget'];
	}

} // class Site_Widget

?>