<?php


class Location_lang_Widget extends WP_Widget {

    function __construct() {
             parent::__construct('location Lang', // Base ID
			 esc_html__( 'Location lang', 'text_domain' ), // Name
			 array( 'description' => esc_html__( 'lang' ), ) // Args
		     );
	}

 function widget( $args, $instance ) {
        extract($args);
        $title_en = empty( $instance['title_en'] ) ? '' : $instance['title_en'] ;
        $title_fr = empty( $instance['title_fr'] ) ? '' : $instance['title_fr'] ;

        // THIS PART CAN BE GREATLY OPTIMIZED
        $lingo = qtrans_getLanguage();
        if ( 'en' == $lingo )
            $title = $title_en;
        else
            $title = $title_fr;

        echo $before_widget;
        if ( $title )
            echo $before_title . $title . $after_title;

        // Use current theme search form if it exists
        get_search_form();

        echo $after_widget;
    }

    function form( $instance ) {
        $instance = wp_parse_args( (array) $instance, array( 'title_en' => '', 'title_fr' => '') );
        $title_en = $instance['title_en'];
        $title_fr = $instance['title_fr'];
?>
        <p><label for="<?php echo $this->get_field_id('title_en'); ?>"><?php _e('Title English:'); ?> <input class="widefat" id="<?php echo $this->get_field_id('title_en'); ?>" name="<?php echo $this->get_field_name('title_en'); ?>" type="text" value="<?php echo esc_attr($title_en); ?>" /></label></p>
    <p><label for="<?php echo $this->get_field_id('title_fr'); ?>"><?php _e('Title Francais:'); ?> <input class="widefat" id="<?php echo $this->get_field_id('title_fr'); ?>" name="<?php echo $this->get_field_name('title_fr'); ?>" type="text" value="<?php echo esc_attr($title_fr); ?>" /></label></p>
<?php
    }

    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $new_instance = wp_parse_args((array) $new_instance, array( 'title_en' => '', 'title_fr' => ''));
        $instance['title_en'] = strip_tags($new_instance['title_en']);
        $instance['title_fr'] = strip_tags($new_instance['title_fr']);
        return $instance;
    }

} // class Location_Widget

?>