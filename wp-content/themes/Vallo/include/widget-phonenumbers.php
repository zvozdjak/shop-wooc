<?php


class Phone_Widget extends WP_Widget {

    function __construct() {
             parent::__construct('phone_widget', // Base ID
			 esc_html__( 'Phone numbers', 'text_domain' ), // Name
			 array( 'description' => esc_html__( 'індивідуальний віджет для телефонних номерів зверху' ), ) // Args
		     );
	}

	
	/**
	 * Back-end widget form
	 * @see WP_Widget::form()
	 * @param array $instance Previously saved values from database.
	 */
     
	public function form( $instance ) {
		$image = ! empty( $instance['image'] ) ? $instance['image'] : esc_html__( 'New image', 'text_domain' );
        $number = ! empty( $instance['number'] ) ? $instance['number'] : esc_html__( 'New phone number', 'text_domain' );
        $url = ! empty( $instance['url'] ) ? $instance['url'] : esc_html__( 'New phone url', 'text_domain' );
        $country = ! empty( $instance['country'] ) ? $instance['country'] : esc_html__( 'New country', 'text_domain' );
		?>
        <style>.gallery{
            display: none;
        }</style>
        <p>
           <label for="<?php echo esc_attr( $this->get_field_id( 'image' ) ); ?>">
           <?php esc_attr_e( 'image:', 'text_domain' ); ?></label> 
		   <input class="widefat src_of_image" id="<?php echo esc_attr( $this->get_field_id( 'image' ) ); ?>" 
           name="<?php echo esc_attr( $this->get_field_name( 'image' ) ); ?>" type="text" 
           value="<?php echo esc_attr( $image ); ?>"/>
           <label for="<?php echo esc_attr( $this->get_field_id( 'buttonus' ) ); ?>"><div class="buttonus"><?php esc_attr_e( 'Галерея', 'text_domain' ); ?> </div></label>
           
           <p>
           <?php 
           $gallery_shortcode = '[gallery id="' . intval( $post->post_parent ) . '" order="DESC" orderby="ID" link="none"]';
           print apply_filters( 'the_content', $gallery_shortcode ); ?>
           </p>
           
        </p>
        
        <p>
		   <label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>">
           <?php esc_attr_e( 'number:', 'text_domain' ); ?></label> 
		   <input class="widefat src_of_number" id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" 
           name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" 
           value="<?php echo esc_attr( $number ); ?>"/>
           <p>
		       <label for="<?php echo esc_attr( $this->get_field_id( 'url' ) ); ?>">
               <?php esc_attr_e( 'url:', 'text_domain' ); ?></label> 
		       <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'url' ) ); ?>" 
               name="<?php echo esc_attr( $this->get_field_name( 'url' ) ); ?>" type="text" 
               value="<?php echo esc_attr( $url ); ?>"/>
		   </p>
        </p>
        
        <p>
           <label for="<?php echo esc_attr( $this->get_field_id( 'country' ) ); ?>">
           <?php esc_attr_e( 'country', 'text_domain' ); ?></label> 
	       <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'country' ) ); ?>" 
           name="<?php echo esc_attr( $this->get_field_name( 'country' ) ); ?>" type="text" 
           value="<?php echo esc_attr( $country ); ?>"/>
		</p>
        
        
        
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 * @see WP_Widget::update()
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['image'] = ( ! empty( $new_instance['image'] ) ) ? strip_tags( $new_instance['image'] ) : '';
        $instance['number'] = ( ! empty( $new_instance['number'] ) ) ? strip_tags( $new_instance['number'] ) : '';
        $instance['url'] = ( ! empty( $new_instance['url'] ) ) ? strip_tags( $new_instance['url'] ) : '';
        $instance['country'] = ( ! empty( $new_instance['country'] ) ) ? strip_tags( $new_instance['country'] ) : '';
		return $instance;
	}
    
    /**
	 * Front-end display of widget.
	 * @see WP_Widget::widget()
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
        
		if ( ! empty( $instance['number'] ) ) {
			echo '<img height="18" src="'.apply_filters( 'widget_title', $instance['image']).'" width="14"/><a href="tel:'.apply_filters( 'widget_title', $instance['url']).'" >' .apply_filters( 'widget_title', $instance['number']). '</a>'  .apply_filters( 'widget_title', $instance['country']). '' ;
		}
		
		echo $args['after_widget'];
	}

} // class Phone_Widget

?> 