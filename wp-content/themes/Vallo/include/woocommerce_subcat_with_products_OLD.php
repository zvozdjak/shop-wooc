<?php
	/**
     * Display product sub categories as thumbnails.
     *
     * @subpackage  Loop
     * @param array $args
     * @return null|boolean
     */
    function woocommerce_product_subcategories_with_products( $args = array() ) {

        global $wp_query;

        $defaults = array(
            'before'        => '',
            'after'         => '',
            'force_display' => false
        );

        $args = wp_parse_args( $args, $defaults );

        extract( $args );

        // Main query only
        if ( ! is_main_query() && ! $force_display ) {
            return;
        }

        // Don't show when filtering, searching or when on page > 1 and ensure we're on a product archive
        if ( is_search() || is_filtered() || is_paged() || ( ! is_product_category() && ! is_shop() ) ) {
            return;
        }

        // Check categories are enabled
        if ( is_shop() && '' === get_option( 'woocommerce_shop_page_display' ) ) {
            return;
        }

        // Find the category + category parent, if applicable
        $term           = get_queried_object();
        $parent_id      = empty( $term->term_id ) ? 0 : $term->term_id;

        if ( is_product_category() ) {
            $display_type = get_woocommerce_term_meta( $term->term_id, 'display_type', true );

            switch ( $display_type ) {
                case 'products' :
                    return;
                break;
                case '' :
                    if ( '' === get_option( 'woocommerce_category_archive_display' ) ) {
                        return;
                    }
                break;
            }
        }

        // NOTE: using child_of instead of parent - this is not ideal but due to a WP bug ( https://core.trac.wordpress.org/ticket/15626 ) pad_counts won't work
        $product_categories = get_categories( apply_filters( 'woocommerce_product_subcategories_args', array(
            'parent'       => $parent_id,
            'menu_order'   => 'ASC',
            'hide_empty'   => 0,
            'hierarchical' => 1,
            'taxonomy'     => 'product_cat',
            'pad_counts'   => 1
        ) ) );
        


        if ( ! apply_filters( 'woocommerce_product_subcategories_hide_empty', false ) ) {
            $product_categories = wp_list_filter( $product_categories, array( 'count' => 0 ), 'NOT' );
        }

        if ( $product_categories ) {
            echo $before;
            

            foreach ( $product_categories as $category ) {
                $category_id = $category->term_id;   
                $thumbnail_id = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );
        	    $image = wp_get_attachment_url( $thumbnail_id );
                
                ?><div class="row">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12"> <?php
								if($parent_id !=0 ) {
                                    echo '<img  src="'. $image .'" alt="" class="pull-left thumbnail img-responsive">';
                                }else{
                                    echo '<a href="'. get_term_link( $category_id, 'product_cat' ).'"><img  src="'. $image .'" alt="" class="pull-left thumbnail img-responsive"></a>';
                                }
                                
                                ?>
							</div>
                
                
                
                <?php
                /*
                wc_get_template( 'content-product_cat.php', array(
                    'category' => $category
                ) );
                */
?>

<div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
<?php 
if($parent_id !=0 ) {
    echo '<h4 class="text-left">'. $category->name .'</h4>';
}else{
    echo '<a href="'. get_term_link( $category_id, 'product_cat' ).'"><h4 class="text-left">'. $category->name .'</h4></a>';
}
?>


								<div class="row">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <?php 
                                echo $category->description;
                                ?></div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									</div>
									<div class="clearfix"></div>

							       <?php
                                   
                                                                      
    if($parent_id != 0 ) { ?>
    <div class="col-lg-7 col-md-8 col-sm-12 col-xs-12">                                        
	<?php $args = array( 'post_type' => 'product', 'posts_per_page' => 10, 'product_cat' => $category->slug, 'orderby' => 'date' );
        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
        
                    <strong class="prodincat"><a href="<?php echo get_permalink( $loop->post->ID ) ?>" title="<?php echo esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID); ?>">
<?php the_title(); ?>&nbsp;<span class="text-right">&gt;&gt;</span></a></strong>
<?php endwhile; ?>
</div>
<?php }
?>

								</div>
							</div>
<?php
                #display products
                #woocommerce_template_loop_product_thumbnail_VALLO();
            ?>
            </div>
            <?php
            
            } 

            // If we are hiding products disable the loop and pagination
            if ( is_product_category() ) {
                $display_type = get_woocommerce_term_meta( $term->term_id, 'display_type', true );

                switch ( $display_type ) {
                    case 'subcategories' :
                        $wp_query->post_count    = 0;
                        $wp_query->max_num_pages = 0;
                    break;
                    case '' :
                        if ( 'subcategories' === get_option( 'woocommerce_category_archive_display' ) ) {
                            $wp_query->post_count    = 0;
                            $wp_query->max_num_pages = 0;
                        }
                    break;
                }
            }

            if ( is_shop() && 'subcategories' === get_option( 'woocommerce_shop_page_display' ) ) {
                $wp_query->post_count    = 0;
                $wp_query->max_num_pages = 0;
            }

            echo $after;

            return true;
        }
        
    }

?>