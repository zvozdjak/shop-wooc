<?php


class About_content extends WP_Widget {

    function __construct() {
             parent::__construct('about_content', // Base ID
			 esc_html__( 'About, content', 'text_domain' ), // Name
			 array( 'description' => esc_html__( 'наповнення контентом' ), ) // Args
		     );
	}

	
	/**
	 * Back-end widget form
	 * @see WP_Widget::form()
	 * @param array $instance Previously saved values from database.
	 */
     
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'New title', 'text_domain' );
        $description = ! empty( $instance['description'] ) ? $instance['description'] : esc_html__( '', 'text_domain' );


		?>
        <style>
        #blurb,
        .wp-editor-area{
            resize: both;
        }
        </style>
        
		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'title:', 'text_domain' ); ?></label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
        <p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'description' ) ); ?>"><?php esc_attr_e( 'description:', 'text_domain' ); ?></label> 
		<textarea class="widefat wp-editor-area" id="<?php echo esc_attr( $this->get_field_id( 'description' ) ); ?>" 
        name="<?php echo esc_attr( $this->get_field_name( 'description' ) ); ?>" 
        class="black-studio-tinymce wp-editor-area active"><?php echo esc_attr( $description ); ?></textarea>
		</p>

      
       
		<?php 
        /*
       $settings = array( 
	'quicktags' => array( 'buttons' => 'strong,em,del,ul,ol,li,close' ), // note that spaces in this list seem to cause an issue
);


$editor_id = 'editpost';
$content = esc_attr( $description );
wp_editor( $content, $editor_id, $settings );
	*/
    
    
    }

	/**
	 * Sanitize widget form values as they are saved.
	 * @see WP_Widget::update()
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['description'] = ( ! empty( $new_instance['description'] ) ) ? strip_tags( $new_instance['description'] ) : '';     

		return $instance;
	}
    
    /**
	 * Front-end display of widget.
	 * @see WP_Widget::widget()
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo '<h4>'.apply_filters( 'widget_title', $instance['title']).'</h4>' ;
		}
        if ( ! empty( $instance['description'] ) ) {
			echo ''.apply_filters( 'widget_title', $instance['description']).'' ;
		}
        
		#echo esc_html__( 'Hello, World!', 'text_domain' );
		echo $args['after_widget'];
	}

} // class Foo_Widget

?>