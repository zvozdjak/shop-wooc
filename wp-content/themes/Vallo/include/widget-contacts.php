<?php


class Contacts_Widget extends WP_Widget {

    function __construct() {
             parent::__construct('contacts_widget', // Base ID
			 esc_html__( 'Footer contacts numbers', 'text_domain' ), // Name
			 array( 'description' => esc_html__( 'індивідуальний віджет для телефонних номерів підвалу' ), ) // Args
		     );
	}

	
	/**
	 * Back-end widget form
	 * @see WP_Widget::form()
	 * @param array $instance Previously saved values from database.
	 */
     
	public function form( $instance ) {
        $number = ! empty( $instance['number'] ) ? $instance['number'] : esc_html__( 'New phone number', 'text_domain' );
        $url = ! empty( $instance['url'] ) ? $instance['url'] : esc_html__( 'New phone url', 'text_domain' );
        $fax = ! empty( $instance['fax'] ) ? $instance['fax'] : esc_html__( 'New fax', 'text_domain' );
        $furl = ! empty( $instance['furl'] ) ? $instance['furl'] : esc_html__( 'New fax url', 'text_domain' );
        ?>
        
        <p>
		   <label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>">
           <?php esc_attr_e( 'number:', 'text_domain' ); ?></label> 
		   <input class="widefat src_of_number" id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" 
           name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" 
           value="<?php echo esc_attr( $number ); ?>"/>
           <p>
		       <label for="<?php echo esc_attr( $this->get_field_id( 'url' ) ); ?>">
               <?php esc_attr_e( 'url:', 'text_domain' ); ?></label> 
		       <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'url' ) ); ?>" 
               name="<?php echo esc_attr( $this->get_field_name( 'url' ) ); ?>" type="text" 
               value="<?php echo esc_attr( $url ); ?>"/>
		   </p>
        </p>
        
        <p>
		   <label for="<?php echo esc_attr( $this->get_field_id( 'fax' ) ); ?>">
           <?php esc_attr_e( 'fax:', 'text_domain' ); ?></label> 
		   <input class="widefat src_of_fax" id="<?php echo esc_attr( $this->get_field_id( 'fax' ) ); ?>" 
           name="<?php echo esc_attr( $this->get_field_name( 'fax' ) ); ?>" type="text" 
           value="<?php echo esc_attr( $fax ); ?>"/>
           <p>
		       <label for="<?php echo esc_attr( $this->get_field_id( 'furl' ) ); ?>">
               <?php esc_attr_e( 'furl:', 'text_domain' ); ?></label> 
		       <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'furl' ) ); ?>" 
               name="<?php echo esc_attr( $this->get_field_name( 'furl' ) ); ?>" type="text" 
               value="<?php echo esc_attr( $furl ); ?>"/>
		   </p>
        </p>
            
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 * @see WP_Widget::update()
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
        $instance['number'] = ( ! empty( $new_instance['number'] ) ) ? strip_tags( $new_instance['number'] ) : '';
        $instance['url'] = ( ! empty( $new_instance['url'] ) ) ? strip_tags( $new_instance['url'] ) : '';
        $instance['fax'] = ( ! empty( $new_instance['fax'] ) ) ? strip_tags( $new_instance['fax'] ) : '';
        $instance['furl'] = ( ! empty( $new_instance['furl'] ) ) ? strip_tags( $new_instance['furl'] ) : '';
		return $instance;
	}
    
    /**
	 * Front-end display of widget.
	 * @see WP_Widget::widget()
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
        
		if ( ! empty( $instance['number'] ) ) {
            echo ' <a href="tel: '.apply_filters( 'widget_title', $instance['url']).'" >Tel.: ' .apply_filters( 'widget_title', $instance['number']). '</a></br> 
                   <a href="tel: '.apply_filters( 'widget_title', $instance['furl']).'" >Fax.: ' .apply_filters( 'widget_title', $instance['fax']). '</a>' ;
		}
		
		echo $args['after_widget'];
	}

} // class Phone_Widget

?> 