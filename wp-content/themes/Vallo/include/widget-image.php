<?php


class Image_Widget extends WP_Widget {

    function __construct() {
             parent::__construct('image', // Base ID
			 esc_html__( 'Image', 'text_domain' ), // Name
			 array( 'description' => esc_html__( 'індивідуальний віджет для виводу зображень' ), ) // Args
		     );
	}

	
	/**
	 * Back-end widget form
	 * @see WP_Widget::form()
	 * @param array $instance Previously saved values from database.
	 */
     
	public function form( $instance ) {
		$image = ! empty( $instance['image'] ) ? $instance['image'] : esc_html__( 'New image', 'text_domain' );
		?>
        <style>.gallery{
            display: none;
        }</style>
        <p>
           <label for="<?php echo esc_attr( $this->get_field_id( 'image' ) ); ?>">
           <?php esc_attr_e( 'image:', 'text_domain' ); ?></label> 
		   <input class="widefat src_of_image" id="<?php echo esc_attr( $this->get_field_id( 'image' ) ); ?>" 
           name="<?php echo esc_attr( $this->get_field_name( 'image' ) ); ?>" type="text" 
           value="<?php echo esc_attr( $image ); ?>"/>
           <label for="<?php echo esc_attr( $this->get_field_id( 'buttonus' ) ); ?>"><div class="buttonus"><?php esc_attr_e( 'Галерея', 'text_domain' ); ?> </div></label>
           <p>
           <?php 
           $gallery_shortcode = '[gallery id="' . intval( $post->post_parent ) . '" order="DESC" orderby="ID" link="none"]';
           print apply_filters( 'the_content', $gallery_shortcode ); ?>
           </p>
           
        </p>
        
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 * @see WP_Widget::update()
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['image'] = ( ! empty( $new_instance['image'] ) ) ? strip_tags( $new_instance['image'] ) : '';
		return $instance;
	}
    
    /**
	 * Front-end display of widget.
	 * @see WP_Widget::widget()
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
        
		if ( ! empty( $instance['image'] ) ) {
			echo '<img height="35" src="'.apply_filters( 'widget_title', $instance['image']).'" width="23"/> ' ;
		}
		
		echo $args['after_widget'];
	}

} // class Phone_Widget

?> 