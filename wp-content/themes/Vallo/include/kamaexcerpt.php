<?php

/**
 * @author phpdesigner
 * @copyright 2016
 */



function kama_excerpt( $args = '' ){
	global $post;

	$default = array(
		'maxchar'     => 350, // ���������� ��������.
		'text'        => '',  // ����� ����� �������� (�� ��������� post_excerpt, ���� ��� post_content.
							  // ���� ���� ��� <!--more-->, �� maxchar ������������ � ������� ��� �� <!--more--> ������ � HTML
		'save_format' => false, // ��������� ������� ����� ��� ���. ���� � �������� ������� ����, �� ��� �� ����� ����������: ��. '<strong><a>'
		'more_text'   => '������ ���...', // ����� ������ ������ ������
		'echo'        => true, // �������� �� ����� ��� ���������� (return) ��� ���������.
	);

	if( is_array($args) )
		$rgs = $args;
	else
		parse_str( $args, $rgs );

	$args = array_merge( $default, $rgs );

	extract( $args );

	if( ! $text ){
		$text = $post->post_excerpt ? $post->post_excerpt : $post->post_content;

		$text = preg_replace ('~\[[^\]]+\]~', '', $text ); // ������� �������, ��������:[singlepic id=3]
		// $text = strip_shortcodes( $text ); // ��� ����� ��� �������� �������, ��� ����� ������� ������ � ����������� ������ ������ ����
		// � ������ �� ������� ������� ���������������� � WordPress. � ��� �������� �������, �� ��� � ������� ��� ������ ���������� 
		// (���� ��� ����� ��������� ����� � �������� ����� ������� �� 50000 ����������)

		// ��� ���� <!--more-->
		if( ! $post->post_excerpt && strpos( $post->post_content, '<!--more-->') ){
			preg_match ('~(.*)<!--more-->~s', $text, $match );
			$text = trim( $match[1] );
			$text = str_replace("\r", '', $text );
			$text = preg_replace( "~\n\n+~s", "</p><p>", $text );

			$more_text = $more_text ? '<a class="kexc_moretext" href="'. get_permalink( $post->ID ) .'#more-'. $post->ID .'">'. $more_text .'</a>' : '';

			$text = '<p>'. str_replace( "\n", '<br />', $text ) .' '. $more_text .'</p>';

			if( $echo )
				return print $text;

			return $text;
		}
		elseif( ! $post->post_excerpt )
			$text = strip_tags( $text, $save_format );
	}   

	// ��������
	if ( mb_strlen( $text ) > $maxchar ){
		$text = mb_substr( $text, 0, $maxchar );
		$text = preg_replace('@(.*)\s[^\s]*$@s', '\\1 ...', $text ); // ������� ��������� �����, ��� 99% ��������
	}

	// ��������� �������� �����. ���������� ������ wpautop()
	if( $save_format ){
		$text = str_replace("\r", '', $text );
		$text = preg_replace("~\n\n+~", "</p><p>", $text );
		$text = "<p>". str_replace ("\n", "<br />", trim( $text ) ) ."</p>";
	}

	//$out = preg_replace('@\*[a-z0-9-_]{0,15}\*@', '', $out); // ������� *some_name-1* - ������ �������

	if( $echo ) return print $text;

	return $text;
}
     ?>
