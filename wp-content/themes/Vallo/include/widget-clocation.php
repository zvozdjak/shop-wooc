<?php


class CLocation_Widget extends WP_Widget {

    function __construct() {
             parent::__construct('clocation', // Base ID
			 esc_html__( 'Contact Location', 'text_domain' ), // Name
			 array( 'description' => esc_html__( 'індивідуальний віджет для виводу адреси на сторінці Контакти' ), ) // Args
		     );
	}

	
	/**
	 * Back-end widget form
	 * @see WP_Widget::form()
	 * @param array $instance Previously saved values from database.
	 */
     
	public function form( $instance ) {
		$country = ! empty( $instance['country'] ) ? $instance['country'] : esc_html__( 'New country', 'text_domain' );
        $address = ! empty( $instance['address'] ) ? $instance['address'] : esc_html__( 'New address', 'text_domain' );
        $city = ! empty( $instance['city'] ) ? $instance['city'] : esc_html__( 'New city', 'text_domain' );      
		$number = ! empty( $instance['number'] ) ? $instance['number'] : esc_html__( 'New phone number', 'text_domain' );
        $url = ! empty( $instance['url'] ) ? $instance['url'] : esc_html__( 'New phone url', 'text_domain' );
        $fax = ! empty( $instance['fax'] ) ? $instance['fax'] : esc_html__( 'New fax', 'text_domain' );
        $furl = ! empty( $instance['furl'] ) ? $instance['furl'] : esc_html__( 'New fax url', 'text_domain' );
        ?>
  
        <p>
           <label for="<?php echo esc_attr( $this->get_field_id( 'country' ) ); ?>">
           <?php esc_attr_e( 'country', 'text_domain' ); ?></label> 
	       <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'country' ) ); ?>" 
           name="<?php echo esc_attr( $this->get_field_name( 'country' ) ); ?>" type="text" 
           value="<?php echo esc_attr( $country ); ?>"/>
		</p>
        <p>
           <label for="<?php echo esc_attr( $this->get_field_id( 'address' ) ); ?>">
           <?php esc_attr_e( 'address', 'text_domain' ); ?></label> 
	       <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'address' ) ); ?>" 
           name="<?php echo esc_attr( $this->get_field_name( 'address' ) ); ?>" type="text" 
           value="<?php echo esc_attr( $address ); ?>"/>
		</p>    
        <p>
           <label for="<?php echo esc_attr( $this->get_field_id( 'city' ) ); ?>">
           <?php esc_attr_e( 'city', 'text_domain' ); ?></label> 
	       <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'city' ) ); ?>" 
           name="<?php echo esc_attr( $this->get_field_name( 'city' ) ); ?>" type="text" 
           value="<?php echo esc_attr( $city ); ?>"/>
		</p>      
        <p>
		   <label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>">
           <?php esc_attr_e( 'number:', 'text_domain' ); ?></label> 
		   <input class="widefat src_of_number" id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" 
           name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" 
           value="<?php echo esc_attr( $number ); ?>"/>
           <p>
		       <label for="<?php echo esc_attr( $this->get_field_id( 'url' ) ); ?>">
               <?php esc_attr_e( 'url:', 'text_domain' ); ?></label> 
		       <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'url' ) ); ?>" 
               name="<?php echo esc_attr( $this->get_field_name( 'url' ) ); ?>" type="text" 
               value="<?php echo esc_attr( $url ); ?>"/>
		   </p>
        </p>
        
        <p>
		   <label for="<?php echo esc_attr( $this->get_field_id( 'fax' ) ); ?>">
           <?php esc_attr_e( 'fax:', 'text_domain' ); ?></label> 
		   <input class="widefat src_of_fax" id="<?php echo esc_attr( $this->get_field_id( 'fax' ) ); ?>" 
           name="<?php echo esc_attr( $this->get_field_name( 'fax' ) ); ?>" type="text" 
           value="<?php echo esc_attr( $fax ); ?>"/>
           <p>
		       <label for="<?php echo esc_attr( $this->get_field_id( 'furl' ) ); ?>">
               <?php esc_attr_e( 'furl:', 'text_domain' ); ?></label> 
		       <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'furl' ) ); ?>" 
               name="<?php echo esc_attr( $this->get_field_name( 'furl' ) ); ?>" type="text" 
               value="<?php echo esc_attr( $furl ); ?>"/>
		   </p>
        </p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 * @see WP_Widget::update()
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['country'] = ( ! empty( $new_instance['country'] ) ) ? strip_tags( $new_instance['country'] ) : '';
        $instance['address'] = ( ! empty( $new_instance['address'] ) ) ? strip_tags( $new_instance['address'] ) : '';
        $instance['city'] = ( ! empty( $new_instance['city'] ) ) ? strip_tags( $new_instance['city'] ) : '';    
		$instance['number'] = ( ! empty( $new_instance['number'] ) ) ? strip_tags( $new_instance['number'] ) : '';
        $instance['url'] = ( ! empty( $new_instance['url'] ) ) ? strip_tags( $new_instance['url'] ) : '';
        $instance['fax'] = ( ! empty( $new_instance['fax'] ) ) ? strip_tags( $new_instance['fax'] ) : '';
        $instance['furl'] = ( ! empty( $new_instance['furl'] ) ) ? strip_tags( $new_instance['furl'] ) : '';
        return $instance;
	}
    
    /**
	 * Front-end display of widget.
	 * @see WP_Widget::widget()
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
        
		if ( ! empty( $instance['city'] ) ) {
			echo ' <tr><td><h5>'.apply_filters( 'widget_title', $instance['country']).'</h5></td></tr> 
                   <tr><td><p>'.apply_filters( 'widget_title', $instance['address']).'</p>
                   <p>'.apply_filters( 'widget_title', $instance['city']).'</p> 
                   <p><a href="tel: '.apply_filters( 'widget_title', $instance['url']).'" >
                   Tel.: ' .apply_filters( 'widget_title', $instance['number']). '</a></p> 
                   <p><a href="tel: '.apply_filters( 'widget_title', $instance['furl']).'" >
                   Fax.: ' .apply_filters( 'widget_title', $instance['fax']). '</a></p></td></tr> ' ;
		}
		#echo esc_html__( 'Hello, World!', 'text_domain' );
		
		
		
		echo $args['after_widget'];
	}

} // class CLocation_Widget

?>