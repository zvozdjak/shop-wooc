<?php


class Location_Widget extends WP_Widget {

    function __construct() {
             parent::__construct('location', // Base ID
			 esc_html__( 'Location', 'text_domain' ), // Name
			 array( 'description' => esc_html__( 'індивідуальний віджет для виводу адреси в підвалі' ), ) // Args
		     );
	}

	
	/**
	 * Back-end widget form
	 * @see WP_Widget::form()
	 * @param array $instance Previously saved values from database.
	 */
     
	public function form( $instance ) {
		$country = ! empty( $instance['country'] ) ? $instance['country'] : esc_html__( 'New country', 'text_domain' );
        $address = ! empty( $instance['address'] ) ? $instance['address'] : esc_html__( 'New address', 'text_domain' );
        $city = ! empty( $instance['city'] ) ? $instance['city'] : esc_html__( 'New city', 'text_domain' );      
		?>
  
        <p>
           <label for="<?php echo esc_attr( $this->get_field_id( 'country' ) ); ?>">
           <?php esc_attr_e( 'country', 'text_domain' ); ?></label> 
	       <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'country' ) ); ?>" 
           name="<?php echo esc_attr( $this->get_field_name( 'country' ) ); ?>" type="text" 
           value="<?php echo esc_attr( $country ); ?>"/>
		</p>
        <p>
           <label for="<?php echo esc_attr( $this->get_field_id( 'address' ) ); ?>">
           <?php esc_attr_e( 'address', 'text_domain' ); ?></label> 
	       <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'address' ) ); ?>" 
           name="<?php echo esc_attr( $this->get_field_name( 'address' ) ); ?>" type="text" 
           value="<?php echo esc_attr( $address ); ?>"/>
		</p>    
        <p>
           <label for="<?php echo esc_attr( $this->get_field_id( 'city' ) ); ?>">
           <?php esc_attr_e( 'city', 'text_domain' ); ?></label> 
	       <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'city' ) ); ?>" 
           name="<?php echo esc_attr( $this->get_field_name( 'city' ) ); ?>" type="text" 
           value="<?php echo esc_attr( $city ); ?>"/>
		</p>      
        
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 * @see WP_Widget::update()
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['country'] = ( ! empty( $new_instance['country'] ) ) ? strip_tags( $new_instance['country'] ) : '';
        $instance['address'] = ( ! empty( $new_instance['address'] ) ) ? strip_tags( $new_instance['address'] ) : '';
        $instance['city'] = ( ! empty( $new_instance['city'] ) ) ? strip_tags( $new_instance['city'] ) : '';    
		return $instance;
	}
    
    /**
	 * Front-end display of widget.
	 * @see WP_Widget::widget()
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
        
		if ( ! empty( $instance['city'] ) ) {
			echo ' <strong>'.apply_filters( 'widget_title', $instance['country']).'</strong> 
                   '.apply_filters( 'widget_title', $instance['address']).'</br>
                   '.apply_filters( 'widget_title', $instance['city']).' ' ;
		}
		#echo esc_html__( 'Hello, World!', 'text_domain' );
		
		
		
		echo $args['after_widget'];
	}

} // class Location_Widget

?>