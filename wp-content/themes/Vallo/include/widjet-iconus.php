<?php

/**
 * Adds Foo_Widget widget.
 */
class Foo_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'foo_widget', // Base ID
			esc_html__( '-- ������, ���������', 'text_domain' ), // Name
			array( 'description' => esc_html__( '������������� ����� ��� ����� ������' ), ) // Args
		);
	}

	

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$url = ! empty( $instance['url'] ) ? $instance['url'] : esc_html__( 'New url', 'text_domain' );
        $image = ! empty( $instance['image'] ) ? $instance['image'] : esc_html__( 'New image', 'text_domain' );
		?>
        
		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'url' ) ); ?>"><?php esc_attr_e( 'url:', 'text_domain' ); ?></label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'url' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'url' ) ); ?>" type="text" value="<?php echo esc_attr( $url ); ?>">
		</p>
        <p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'image' ) ); ?>"><?php esc_attr_e( 'image:', 'text_domain' ); ?></label> 
		<input class="widefat src_of_image" id="<?php echo esc_attr( $this->get_field_id( 'image' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'image' ) ); ?>" type="text" value="<?php echo esc_attr( $image ); ?>">

        <?php 
        $gallery_shortcode = '[gallery id="' . intval( $post->post_parent ) . '" order="DESC" orderby="ID" link="none"]';
        print apply_filters( 'the_content', $gallery_shortcode ); ?>

        </p>
        
        
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['url'] = ( ! empty( $new_instance['url'] ) ) ? strip_tags( $new_instance['url'] ) : '';
        $instance['image'] = ( ! empty( $new_instance['image'] ) ) ? strip_tags( $new_instance['image'] ) : '';        

		return $instance;
	}
    
    /**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		if ( ! empty( $instance['image'] ) ) {
			echo '<a href="'.apply_filters( 'widget_title', $instance['url']).'" ><img src="'.apply_filters( 'widget_title', $instance['image']).'"/></a>' ;
		}
		#echo esc_html__( 'Hello, World!', 'text_domain' );
		echo $args['after_widget'];
	}

} // class Foo_Widget

?>