<?php
function woocommerce_get_product_thumbnail_VALLO( $size = 'shop_catalog', $deprecated1 = 0, $deprecated2 = 0 ) {
        global $post;
        $image_size = apply_filters( 'single_product_archive_thumbnail_size', $size );

        if ( has_post_thumbnail() ) {
            $props = wc_get_product_attachment_props( get_post_thumbnail_id(), $post );
            return get_the_post_thumbnail( $post->ID, $image_size, array(
                'title'  => $props['title'],
                'alt'    => $props['alt'],
                'class' => 'pull-left thumbnail img-responsive',
            ) );
        } elseif ( wc_placeholder_img_src() ) {
            return '<img src="" alt="" class="pull-left thumbnail img-responsive">';
        }
    }
?>