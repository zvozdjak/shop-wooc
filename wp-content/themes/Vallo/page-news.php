<?php

/**
 *Template Name: News
 */


get_header(); ?>



	<div class="main">

		<section>
				<div class="row">
					<hr>
					<div class="col-lg-5 col-md-7 col-sm-12 col-xs-12">
                    <h4><?php echo __('News') ?></h4>
<!--вивід всіх записів з категорією Новини в лівій колонці на сторінці новин-->                    
<?php
global $post;
$args = array( 'category' => 221 );
$myposts = get_posts( $args );
  foreach( $myposts as $post ){ setup_postdata($post);
	?>
	<strong><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></strong>
    <p><?php kama_excerpt(); ?>
    <a href="<?php echo get_permalink(); ?>">  <?php echo __('Read more...') ?> → </a></p><br />
	<?php
   }
wp_reset_postdata();
?>
					</div>
                    
					<div class="col-lg-4 col-md-5 col-sm-6 col-xs-12">
						<h4 class="text-right"><?php echo __('Mink breeding') ?></h4>
<!--вивід всіх записів з категорією Стаття в правій колонці на сторінці новин-->                    
<?php
global $post;
$args = array( 'category' => 229 );
$myposts = get_posts( $args );
  foreach( $myposts as $post ){ setup_postdata($post);
	?>
	<strong><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></strong>
    <p><?php kama_excerpt('maxchar=120'); ?>
    <a href="<?php echo get_permalink(); ?>">  <?php echo __('Read more...') ?> → </a></p><br />
	<?php
   }
wp_reset_postdata();
?>                        
					</div>
					
					<div class="clearfix visible-md-block"></div>
                       <?php get_sidebar( 'top_icons' ); ?>
               </div>
				
		</section>
      </div>
	

<?php
get_footer();
?>