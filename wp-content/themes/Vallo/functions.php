<?php

/**
  * @package WordPress
  * @subpackage ITmoms Web-Design Studio
  * @since ITmoms Site Web-Design Studio 1.0
 */

?>




<?php

function my_search_form( $form ) {

	$form = '
	<form class="navbar-form radius hidden-xs" role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
		<label class="screen-reader-text" for="s"> </label>
		<input src="http://shop.intech.lviv.ua/wp-content/uploads/2016/11/minisearch.png" type="image" />
        <input type="text" value="' . get_search_query() . '" name="s" id="s" class="form-control" placeholder="'.__('Search').'" />
	</form>';

	return $form;
}

add_filter( 'get_search_form', 'my_search_form' );

?>




<?php

function my_scripts_method() {
	wp_enqueue_script('jquery');            
}
add_action( 'wp_enqueue_scripts', 'my_scripts_method' ); 

?>




<?php

if ( ! function_exists( 'custom_navigation_menus' ) ) {

// Register Navigation Menus
function custom_navigation_menus() {

	$locations = array(
	);
	register_nav_menus( $locations );

}
add_action( 'init', 'custom_navigation_menus' );

}


/*

 if ( is_active_sidebar( 'top_icons' ) ) : ?>
	<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
		<?php dynamic_sidebar( 'top_icons' ); ?>
	</div><!-- #primary-sidebar -->
<?php endif; 
*/



###
# WIDJETUS 1
# DISPLAY WIDJET area
###
function arphabet_widgets_init() {
    register_sidebar( array(
		'name'          => 'Main Page box',
		'id'            => 'mainpage',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<h1>',
		'after_title'   => '</h1> <hr>',
	) );
    
    register_sidebar( array(
		'name'          => 'About left box',
		'id'            => 'about_left',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) ); 

	register_sidebar( array(
		'name'          => 'About right box',
		'id'            => 'about_right',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );

    
   	register_sidebar( array(
		'name'          => 'Top icons area',
		'id'            => 'top_icons',
		'before_widget' => '<li>',
		'after_widget'  => '</li>',
		'before_title'  => '',
		'after_title'   => '',
	) );        
    
    register_sidebar( array(
		'name'          => 'Top langs area',
		'id'            => 'top_langs',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );  
    
    register_sidebar( array(
		'name'          => 'Social icons area',
		'id'            => 'social_icons',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );
    
    register_sidebar( array(
		'name'          => 'Top phone numbers',
		'id'            => 'phone_numbers',
		'before_widget' => '<li>',
		'after_widget'  => '</li>',
		'before_title'  => '',
		'after_title'   => '',
	) );
    
    register_sidebar( array(
		'name'          => 'Footer locations',
		'id'            => 'location',
		'before_widget' => '<td>',
		'after_widget'  => '</td>',
		'before_title'  => '',
		'after_title'   => '',
	) );

   register_sidebar( array(
		'name'          => 'Footer contacts area',
		'id'            => 'contacts',
		'before_widget' => '<td>',
		'after_widget'  => '</td>',
		'before_title'  => '',
		'after_title'   => '',
	) );

   register_sidebar( array(
		'name'          => 'Footer location image area',
		'id'            => 'image',
		'before_widget' => '<td>',
		'after_widget'  => '</td>',
		'before_title'  => '',
		'after_title'   => '',
	) );

   register_sidebar( array(
		'name'          => 'Footer phone image area',
		'id'            => 'imagetel',
		'before_widget' => '<td>',
		'after_widget'  => '</td>',
		'before_title'  => '',
		'after_title'   => '',
	) );
    
    register_sidebar( array(
		'name'          => 'Footer site area',
		'id'            => 'site',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );
    
    register_sidebar( array(
		'name'          => 'Footer mail area',
		'id'            => 'mail',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );

   register_sidebar( array(
		'name'          => 'Contact page locations',
		'id'            => 'clocation',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );
   
   register_sidebar( array(
		'name'          => 'Contacts of workers',
		'id'            => 'workers',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );
  
   register_sidebar( array(
		'name'          => 'All rights reserved',
		'id'            => 'rights',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );
    /*
    register_sidebar( array(
		'name'          => 'Company_all_rights_reserved',
		'id'            => 'crights',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );
    */
    
    

}
add_action( 'widgets_init', 'arphabet_widgets_init' );


# WIDJETUS 2
require_once('include/widget-icons.php');
require_once('include/widget-phonenumbers.php');
require_once('include/widget-location.php');
require_once('include/widget-location_lang.php');
require_once('include/widget-contacts.php');
require_once('include/widget-image.php');
require_once('include/widget-mail.php');
require_once('include/widget-clocation.php');
require_once('include/widget-workers.php');
// widjet for about
require_once('include/widget-about_content.php');
require_once('include/widget-sites.php');




###
# WIDJETUS 3
# JQuery of widjets
###
add_action( 'admin_enqueue_scripts', 'my_custom_admin_script' );
function my_custom_admin_script() {
    if (!is_admin()) {
        
    }
    if(is_admin()){
        wp_enqueue_script('custom_admin_script', get_bloginfo('template_url').'/js/admin_script.js?ver=4.5.8', array('jquery'));
    }   
}

// register  widget
function register_foo_widget() {
    register_widget( 'Foo_Widget' );
    register_widget( 'Phone_Widget' );
    register_widget( 'Location_Widget' );
    register_widget( 'Location_lang_Widget' );
    register_widget( 'Contacts_Widget' );
    register_widget( 'Image_Widget' );
    register_widget( 'Mail_Widget' );
    register_widget( 'CLocation_Widget' );
    register_widget( 'Workers_Widget' );
    register_widget( 'Sites_Widget' );
}
add_action( 'widgets_init', 'register_foo_widget' );

/** 
editor for widget 
**/



###
# Woocommerce
###

// remove default sorting dropdown 
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
// remove default count 
remove_action( 'woocommerce_before_shop_loop' , 'woocommerce_result_count', 20 );

//remove response in single product
remove_action( 'woocommerce_product_tabs', 'woocommerce_product_reviews_tab', 30 );
remove_action( 'woocommerce_product_tab_panels', 'woocommerce_product_reviews_panel', 30 );
###
# add javascript only to single product
###

add_action( 'wp_enqueue_scripts', 'dc_datepicker_options' );

function dc_datepicker_options() {
if(is_product()) {
  $ss_url = get_stylesheet_directory_uri();
  wp_enqueue_script( 'bxslider', "{$ss_url}/js/js_slider/jquery.bxslider.js" );
  wp_enqueue_style( 'bxslider', "{$ss_url}/css/jquery.bxslider.css" );
} else {
    // Returns false 
}
}


//remove sidebar Woo
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10);
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
///


////
// product in the category list
///


/// image thumbnail
#remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
#remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10);
/*REMOVE old loop-title action             */
#remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );


# remove base link to product
#remove_action( 'woocommerce_before_shop_loop_item' );

# function of additional info
## function isa_woocommerce_all_pa()
#require_once('include/woocommerce_productlist_additional_info.php');

# function of thumbnail of products
## function woocommerce_get_product_thumbnail_VALLO()
#require_once('include/woocommerce_productlist_thumbnail.php');

# function of shot description of products
## function woocommerce_template_loop_product_title_with_sku()
#require_once('include/woocommerce_productlist_shot_description.php');



/**
 * функція woocommerce_product_subcategories_with_products
 * */
#require_once('include/woocommerce_subcat_with_products.php');

/**
 * gallery of products  
 **/


/**
 * функція обрізки тексту, заміняє стандартну the_excerpt()
 * */
require_once('include/kamaexcerpt.php');

/**
 * function of breadcrumbs
 */
require_once('include/breadcrumbs.php');
 
 /// for non display related products(29/04)
remove_action ('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

/**
	 * Хук, що забирає можливість додавання відгуку до товару
	 */
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );
 
function woo_remove_product_tabs( $tabs ) {
 
unset( $tabs['reviews'] ); 
 
return $tabs;
 
}


// Change columns number // НеПрацює''
        // ---------------------
        add_filter( 'loop_shop_columns', 'tm_product_columns', 5);
        function tm_product_columns($columns) {
            if ( is_shop() || is_product_category() || is_product_tag() ) {
                $columns = 2;
                return $columns;
            }
        }
  

// Display 25 products per page. Goes in functions.php
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 25;' ), 20 );

// remove add-to-cart button on archive-page
remove_action( 'woocommerce_after_shop_loop_item' , 'woocommerce_template_loop_add_to_cart', 10 );

//change priority of title on content-product?

remove_action( 'woocommerce_shop_loop_item_title' , 'woocommerce_template_loop_product_title' , 10);
add_action ( 'woocommerce_before_shop_loop_item_title_own_code' , 'woocommerce_template_loop_product_title' , 5);

////
// add size attribute for item on content-product
///
add_action( 'woocommerce_before_shop_loop_item_img_own_code', 'img_for_products_of_cat', 10 );
 
function img_for_products_of_cat() {
	global $product;

	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $loop->post->ID ), 'single-post-thumbnail' );
	?>
    <img src="<?php  echo $image[0]; ?>" height="90" class="img_for_products_of_cat" data-id="<?php echo $loop->post->ID; ?>">
<?php
}



add_action( 'woocommerce_before_shop_loop_item_title_own_code', 'bbloomer_show_product_dimensions_loop', 20 );
 
function bbloomer_show_product_dimensions_loop() {
   
global $product;
         
$dimensions = $product->get_dimensions();
         
if ( ! empty( $dimensions ) ) {
             
echo '<div class="dimensions"> ';
if($product->get_length()){
    echo $product->get_length();
}
if($product->get_width()){
    echo ' x '  .$product->get_width();
}
if($product->get_height()){
    echo ' x '  .$product->get_height();
}
echo  get_option( 'woocommerce_dimension_unit' );

echo '</div>';
         
}
 
}

///
// add next/previous product on single page on the bottom
///
add_action( 'woocommerce_after_single_product', 'bbloomer_prev_next_product' );
 
function bbloomer_prev_next_product(){
 
echo '<div class="prev_next_buttons">';
 
    // 'product_cat' will make sure to return next/prev from current category
        $previous = next_post_link('%link', '&larr; PREVIOUS', TRUE, ' ', 'product_cat');
    $next = previous_post_link('%link', 'NEXT &rarr;', TRUE, ' ', 'product_cat');
 
    echo $previous;
    echo $next;
     
echo '</div>';
         
}




//add title of product under image (single-product)
remove_action( 'woocommerce_single_product_summary' , 'woocommerce_template_single_title' , 5 );
add_action( 'woocommerce_before_single_product_summary' , 'woocommerce_template_single_title' , 22);

//add category(product-meta) of product above the image (single-product)
remove_action( 'woocommerce_single_product_summary' ,'woocommerce_template_single_meta' , 40);
add_action ( 'woocommerce_before_single_product', 'woocommerce_template_single_meta' , 5 );

 /**
 * remove price on content-product page
 */
	remove_action( 'woocommerce_after_shop_loop_item_title' , 'woocommerce_template_loop_price' , 10);
    
    /**
 * Non-display "Product Description" @ Single Product Page Tabs - WooCommerce
 * 
 */
 
add_filter('woocommerce_product_description_heading', 'bbloomer_rename_product_description_heading');
 
function bbloomer_rename_product_description_heading() {
return '';
}
	
    /**
 * Move product tabs beside the product image - WooCommerce
 * 
 */
 
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_output_product_data_tabs', 19 );
    

/**
 * зразок-функції
 */
function good_connection() {
   
print "good connection";
 
}

/**
 * form displayer (sidebar)
 */
 
 function form_displayer (){
    ?>
    <h3><?php echo __('Feedback') ?></h3>
<hr />
<?php  

if (qtranxf_getLanguage() == 'en') {
echo do_shortcode('[contact-form-7 id="1530" title="Vallo_contact_form" html_class="" ]');
} else if (qtranxf_getLanguage() == 'ua') {
echo do_shortcode('[contact-form-7 id="1294" title="Vallo_contact_form" html_class="" ]');
} else if (qtranxf_getLanguage() == 'pl') {
echo do_shortcode('[contact-form-7 id="1528" title="Vallo_contact_form" html_class="" ]');
} else if (qtranxf_getLanguage() == 'ru') {
echo do_shortcode('[contact-form-7 id="1527" title="Vallo_contact_form" html_class="" ]');
} else if (qtranxf_getLanguage() == 'dk') {
echo do_shortcode('[contact-form-7 id="1529" title="Vallo_contact_form" html_class="" ]');
} else {
echo do_shortcode('[contact-form-7 id="1530" title="Vallo_contact_form" html_class="" ]');
} 
  
 }
 

	/**
 * redirect to checkout after add to cart button
 
 function bbloomer_redirect_checkout_add_cart( $url ) {
    $url = get_permalink( get_option( 'woocommerce_checkout_page_id' ) ); 
    return $url;
}
 
 
add_filter( 'woocommerce_add_to_cart_redirect', 'bbloomer_redirect_checkout_add_cart' );
*/



/**
 * for display special current-category image on content-product page (sidebar)
 */
  function category_image (){
    ?>
    <h3><?php echo __('Check the range') ?></h3>
    
<?php  
 
if ( is_product_category() ){
	    global $wp_query;
	    $cat = $wp_query->get_queried_object();
	    $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
	    $image = wp_get_attachment_url( $thumbnail_id );
	    if ( $image ) {
		    echo '<img src="' . $image . '" alt="' . $cat->name . '" . class="img-responsive" />';
		}
	}
 
 }
 
 
 /**
 * post image()order_print) on content-product paint-stirrers (sidebar)
 */
  function post_order_print (){
 
if ( is_product_category( '191' ) ) {
    
    ?>
    
  <h3><?php echo __('Add an ad')?></h3> 
  <?php 
  
  $thumbnail = (get_the_post_thumbnail_url('1604'));
 
if ( $thumbnail ) {
    $alt_text = get_post_meta( $thumbnail->ID, '_wp_attachment_image_alt', true );
 
    if ( ! empty( $thumbnail ) ) {
      ?> 
       
        <a href="<?php	echo  get_post_permalink(1604); ?>"> <?php
       echo '<img src="' . $thumbnail .'" alt="'. $thumbnail->ID . '" . class="img-responsive" />';
   ?></a>
   
    <?php   
   }
}
}

elseif (  has_term( 'paint-stirrers', 'product_cat' ) ){
 ?>
    
  <h3><?php echo __('Add an add')?></h3> 
  <?php 
  
  $thumbnail = (get_the_post_thumbnail_url('1604'));
 
if ( $thumbnail ) {
    $alt_text = get_post_meta( $thumbnail->ID, '_wp_attachment_image_alt', true );
 
    if ( ! empty( $thumbnail ) ) {
      ?> 
       
        <a href="<?php	echo  get_post_permalink(1604); ?>"> <?php
       echo '<img src="' . $thumbnail .'" alt="'. $thumbnail->ID . '" . class="img-responsive" />';
   ?></a>
   
    <?php   
   }
}
}


}
 
 
 
  /**
 *  for display related product-category on content-product paint-stirrers (sidebar)
 */
  function related_product_cat (){
    
    ?>
    <h3><?php echo __('Other products') ?></h3>
    <hr />
    <?php 
    if ( is_product_category() ){
     

                    
  $taxonomy     = 'product_cat';
  $orderby      = 'name';  
  $show_count   = 0;      // 1 for yes, 0 for no
  $pad_counts   = 0;      // 1 for yes, 0 for no
  $hierarchical = 1;      // 1 for yes, 0 for no  
   
  $empty        = 0;

  $args = array(
         'taxonomy'     => $taxonomy,
         'orderby'      => $orderby,
         'show_count'   => $show_count,
         'pad_counts'   => $pad_counts,
         'hierarchical' => $hierarchical,
         'exclude' => $GLOBALS['term_cat_id'],
         'hide_empty'   => $empty
  );
 $all_categories = get_categories( $args );

 foreach ($all_categories as $cat) {
    
    if($cat->category_parent == 0) {

        
        $category_id = $cat->term_id;   
        $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
	    $image = wp_get_attachment_url( $thumbnail_id );
        ?>
	   
       <a href="<?php	echo  get_term_link($cat->slug, 'product_cat'); ?>" class="other_category_link"><?php
		    echo '<span class="other_category_title">' . $cat->slug . '</span>' .

		    '<img src="' . $image .'" alt="" class="img-responsive" />';
?></a>
<?php 
   }
   }
 }
 
  }
 
 /**
 *  for display related product-category on content-product paint-stirrers (sidebar)
 */
 
 
 add_action( 'topsidebar_77', 'form_displayer' );// sidebar
 

 /**
	extra fields for woocommerce category
 */


	//Product Cat Create page
function wh_taxonomy_add_new_meta_field() {
    ?>


    <div class="form-field">
        <label for="desc_en"><?php _e('Description EN'); ?></label>
        <textarea name="desc_en" id="desc_en"></textarea>
        
    </div>
    <div class="form-field">
        <label for="desc_dk"><?php _e('Description DK'); ?></label>
        <textarea name="desc_dk" id="desc_dk"></textarea>
        
    </div>
    <?php
}

//Product Cat Edit page
function wh_taxonomy_edit_meta_field($term) {

    //getting term ID
    $term_id = $term->term_id;

    // retrieve the existing value(s) for this meta field.

    $desc_en = get_term_meta($term_id, 'desc_en', true);
    $desc_dk = get_term_meta($term_id, 'desc_dk', true);
    ?>

    <tr class="form-field">
        <th scope="row" valign="top"><label for="desc_en"><?php _e('Description EN'); ?></label></th>
        <td>
            <textarea name="desc_en" id="desc_en"><?php echo esc_attr($desc_en) ? esc_attr($desc_en) : ''; ?></textarea>
            <p class="description"><?php _e('Enter a meta description'); ?></p>
        </td>
    </tr>
    <tr class="form-field">
        <th scope="row" valign="top"><label for="desc_dk"><?php _e('Description DK'); ?></label></th>
        <td>
            <textarea name="desc_dk" id="desc_dk"><?php echo esc_attr($desc_dk) ? esc_attr($desc_dk) : ''; ?></textarea>
            <p class="description"><?php _e('Enter a meta description'); ?></p>
        </td>
    </tr>
    <?php
}

add_action('product_cat_add_form_fields', 'wh_taxonomy_add_new_meta_field', 10, 1);
add_action('product_cat_edit_form_fields', 'wh_taxonomy_edit_meta_field', 10, 1);

// Save extra taxonomy fields callback function.
function wh_save_taxonomy_custom_meta($term_id) {


    $desc_en = filter_input(INPUT_POST, 'desc_en');
    $desc_dk = filter_input(INPUT_POST, 'desc_dk');

    update_term_meta($term_id, 'desc_en', $desc_en);
    update_term_meta($term_id, 'desc_dk', $desc_dk);
}

add_action('edited_product_cat', 'wh_save_taxonomy_custom_meta', 10, 1);
add_action('create_product_cat', 'wh_save_taxonomy_custom_meta', 10, 1);

function current_category_data($post_id){
	$term =  get_the_terms( $post_id, 'product_cat' );

	foreach ($term as $t) {
	   $parentId = $t->parent;
	   if($parentId == 0){
	     return $t;
	   }else{
	     $term = get_terms( 'product_cat', array('include' => array($parentId)) );
	   }
	}
}

function next_post_link_product($format='%link &raquo;', $link='%title', $in_same_cat = false, $excluded_categories = '') {
    adjacent_post_link_product($format, $link, $in_same_cat, $excluded_categories, false);
}

function previous_post_link_product($format='&laquo; %link', $link='%title', $in_same_cat = false, $excluded_categories = '') {
    adjacent_post_link_product($format, $link, $in_same_cat, $excluded_categories, true);
}

function adjacent_post_link_product( $format, $link, $in_same_cat = false, $excluded_categories = '', $previous = true ) {
    if ( $previous && is_attachment() )
        $post = get_post( get_post()->post_parent );
    else
        $post = get_adjacent_post_product( $in_same_cat, $excluded_categories, $previous );

    if ( ! $post ) {
        $output = '';
    } else {
        $title = $post->post_title;

        if ( empty( $post->post_title ) )
            $title = $previous ? __( 'Previous Post' ) : __( 'Next Post' );

        $title = apply_filters( 'the_title', $title, $post->ID );
        $date = mysql2date( get_option( 'date_format' ), $post->post_date );
        $rel = $previous ? 'prev' : 'next';

        $string = '<a href="' . get_permalink( $post ) . '" rel="'.$rel.'">';
        $inlink = str_replace( '%title', $title, $link );
        $inlink = str_replace( '%date', $date, $inlink );
        $inlink = $string . $inlink . '</a>';

        $output = str_replace( '%link', $inlink, $format );
    }

    $adjacent = $previous ? 'previous' : 'next';

    echo apply_filters( "{$adjacent}_post_link", $output, $format, $link, $post );
}

function get_adjacent_post_product( $in_same_cat = false, $excluded_categories = '', $previous = true ) {
    global $wpdb;

    if ( ! $post = get_post() )
        return null;

    $current_post_date = $post->post_date;

    $join = '';
    $posts_in_ex_cats_sql = '';
    if ( $in_same_cat || ! empty( $excluded_categories ) ) {
        $join = " INNER JOIN $wpdb->term_relationships AS tr ON p.ID = tr.object_id INNER JOIN $wpdb->term_taxonomy tt ON tr.term_taxonomy_id = tt.term_taxonomy_id";

        if ( $in_same_cat ) {
            if ( ! is_object_in_taxonomy( $post->post_type, 'product_cat' ) )
                return '';
            $cat_array = wp_get_object_terms($post->ID, 'product_cat', array('fields' => 'ids'));
            if ( ! $cat_array || is_wp_error( $cat_array ) )
                return '';
            $join .= " AND tt.taxonomy = 'product_cat' AND tt.term_id IN (" . implode(',', $cat_array) . ")";
        }

        $posts_in_ex_cats_sql = "AND tt.taxonomy = 'product_cat'";
        if ( ! empty( $excluded_categories ) ) {
            if ( ! is_array( $excluded_categories ) ) {
                // back-compat, $excluded_categories used to be IDs separated by " and "
                if ( strpos( $excluded_categories, ' and ' ) !== false ) {
                    _deprecated_argument( __FUNCTION__, '3.3', sprintf( __( 'Use commas instead of %s to separate excluded categories.' ), "'and'" ) );
                    $excluded_categories = explode( ' and ', $excluded_categories );
                } else {
                    $excluded_categories = explode( ',', $excluded_categories );
                }
            }

            $excluded_categories = array_map( 'intval', $excluded_categories );

            if ( ! empty( $cat_array ) ) {
                $excluded_categories = array_diff($excluded_categories, $cat_array);
                $posts_in_ex_cats_sql = '';
            }

            if ( !empty($excluded_categories) ) {
                $posts_in_ex_cats_sql = " AND tt.taxonomy = 'product_cat' AND tt.term_id NOT IN (" . implode($excluded_categories, ',') . ')';
            }
        }
    }

    $adjacent = $previous ? 'previous' : 'next';
    $op = $previous ? '<' : '>';
    $order = $previous ? 'DESC' : 'ASC';

    $join  = apply_filters( "get_{$adjacent}_post_join", $join, $in_same_cat, $excluded_categories );
    $where = apply_filters( "get_{$adjacent}_post_where", $wpdb->prepare("WHERE p.post_date $op %s AND p.post_type = %s AND p.post_status = 'publish' $posts_in_ex_cats_sql", $current_post_date, $post->post_type), $in_same_cat, $excluded_categories );
    $sort  = apply_filters( "get_{$adjacent}_post_sort", "ORDER BY p.post_date $order LIMIT 1" );

    $query = "SELECT p.id FROM $wpdb->posts AS p $join $where $sort";
    $query_key = 'adjacent_post_' . md5($query);
    $result = wp_cache_get($query_key, 'counts');
    if ( false !== $result ) {
        if ( $result )
            $result = get_post( $result );
        return $result;
    }

    $result = $wpdb->get_var( $query );
    if ( null === $result )
        $result = '';

    wp_cache_set($query_key, $result, 'counts');

    if ( $result )
        $result = get_post( $result );

    return $result;
}

add_action( 'wp_enqueue_scripts', 'mytheme_scripts' );
/**
 * Enqueue Dashicons style for frontend use
 */
function mytheme_scripts() {
	wp_enqueue_style( 'dashicons' );
}
