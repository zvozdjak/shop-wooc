<?php
  /**
  * Файл Index.php
  *
  *
  * виводить контент матеріалів на WordPress
  *
  * @package WordPress
  * @subpackage ITmoms Web-Design Studio
  * @since ITmoms Site Web-Design Studio 1.0
  */
?><!DOCTYPE html>

<!--[if IE 7]> <html class="ie ie7" <?php language_attributes(); ?>> <![endif]--> 
<!--[if IE 8]> <html class="ie ie8" <?php language_attributes(); ?>> <![endif]--> 
<!--[if !(IE 7) | !(IE 8) ]><!--> <html <?php language_attributes(); ?>> <!--<![endif]--> 



<head>

  <meta charset="<?php bloginfo( 'charset' ); ?>"/>

  <meta name="viewport" content="width=device-width, initial-scale=1"/>

  <title><?php wp_title( '|', true, 'right' ); ?>Vallo scetches</title>
  
  <link rel="profile" href="http://gmpg.org/xfn/11"/>

  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>"/>

  <link rel='stylesheet' id='main-style' href='<?php echo get_stylesheet_uri(); ?>?ver=1.3.2' type='text/css' media='all' />
  

 <!-- <link rel="shortcut icon" type="image/ico" href="http:shop.intech.lviv.ua/public_html/favicon.ico" /> -->
 
    <!-- Compiled and minified bootstrap CSS  -->
    <link rel="stylesheet" href="https://yastatic.net/bootstrap/3.3.6/css/bootstrap.min.css"/>
  
    <!-- Compiled and minified bootstrap JS  -->
	<link rel="stylesheet" href="https://yastatic.net/bootstrap/3.3.6/js/bootstrap.min.js"/>
    
    <!-- Minified Jquery JS -->
	<!--<link rel="stylesheet" href=" https://yastatic.net/jquery/3.1.0/jquery.min.js"/>-->
  <script src='https://www.google.com/recaptcha/api.js'></script>   
 
 
  <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
 
 <link rel='stylesheet' id='main-style' href='/wp-content/themes/Vallo/senq.css?ver=1.5' type='text/css' media='all' />

  <?php wp_head(); ?>

</head>

<body >
 <div class="container">
	<header>
		<nav role ="navigation" class="navbar navbar-mini">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<img height="50" src="http://shop.intech.lviv.ua/wp-content/uploads/2016/11/minilogo.png" width="88"/>
                <a href="#" class="navbar-brand visible-xs">Vallo</a>
			</div>
			<!-- Collection of nav links, forms, and other content for toggling -->
			<div id="navbarCollapse" class="collapse navbar-collapse">
				
				<ul class="nav navbar-nav navbar-right">
 
 <?php if ( is_active_sidebar( 'top_icons' ) ) : ?>

 <?php dynamic_sidebar( 'top_icons' ); ?>

 <?php endif; ?>
 
 
 
					<li class="hidden-xs"></li>
					<li>
                        <div class="form-group">	
                    	    <?php get_search_form(); ?>
                        </div>
                    </li>
                    
                    <li class="langs">
                    <?php if ( is_active_sidebar( 'top_langs' ) ) : ?>
                     <?php dynamic_sidebar( 'top_langs' ); ?>                    
                     <?php endif; ?>
                    </li>
					
				</ul>
                 
               
                <div class="nav navbar-nav visible-xs">
					<?php wp_nav_menu( array( 'theme_location' => 'Vallo_pages' ) ); ?>
				</div>
			</div>
		</nav>

		<nav class="navbar navbar-orange hidden-xs">
			    <div class="navbar-header">
					<a class="navbar-brand" href="http://shop.intech.lviv.ua/"><img height="63" src="http://shop.intech.lviv.ua/wp-content/uploads/2016/11/logo.png" width="170"/></a>
				</div>
                
				<?php wp_nav_menu( array( 'menu' => 'top_static_pages', 'menu_class' => 'nav navbar-nav navbar-right' ) ); ?>                
            
		</nav>

		<ul class="left-tel">
        
  <?php if ( is_active_sidebar( 'phone_numbers' ) ) : ?>

  <?php dynamic_sidebar( 'phone_numbers' ); ?>

  <?php endif; ?>
  
			
			
		</ul>
	</header>
    
    <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>