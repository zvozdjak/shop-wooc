<?php

/**
 * @author ITMoms WebStudio
 * @copyright 2016
 */

get_header();

?>
<div class="main">
  <section>
    <div class="row">
          <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
          <?php while ( have_posts() ) : the_post(); ?>
          <strong><?php the_title(); ?></strong>
          <br />
          <?php the_content(); ?>
          <?php endwhile; ?>
          </div>
          <div class="clearfix visible-md-block"></div>
               <?php get_sidebar( 'top_icons' ); ?>
    </div>
  </section>
</div>

<?php get_footer(); ?>