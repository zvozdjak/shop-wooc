<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage test
 * @since test 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
 
 
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
    <link rel="shortcut icon" href="/wp-content/themes/happwell/img/favicon.png" />
    <?php wp_head(); ?>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
     
     <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,800,700,600,300&amp;subset=latin,cyrillic" media="all"/>
    <style type="text/css">
        @font-face {
            font-family: 'FontAwesome';
            src: url('/wp-content/themes/happwell/fonts/fontawesome-webfont.eot?v=4.3.0');
            src: 
            url('/wp-content/themes/happwell/fonts/fontawesome-webfont.eot?#iefix&v=4.3.0') format('embedded-opentype'), 
            url('/wp-content/themes/happwell/fonts/fontawesome-webfont.woff2?v=4.3.0') format('woff2'), 
            url('/wp-content/themes/happwell/fonts/fontawesome-webfont.woff?v=4.3.0') format('woff'), 
            url('/wp-content/themes/happwell/fonts/fontawesome-webfont.ttf?v=4.3.0') format('truetype'), 
            url('/wp-content/themes/happwell/fonts/fontawesome-webfont.svg?v=4.3.0#fontawesomeregular') format('svg');
            font-weight: normal;
            font-style: normal;
        }
    </style>



    <link rel='stylesheet' href='/wp-content/themes/happwell/style.css?ver=7.7' type='text/css' media='all' />
    <link rel='stylesheet' href='/wp-content/themes/happwell/css/pushy.css' type='text/css' media='all' />
    
    <!-- animation add -->
    <link rel="stylesheet" media="all" href="/wp-content/themes/happwell/css/animate.css">
    <script src="/wp-content/themes/happwell/js/wow.min.js"></script>
    <script>new WOW().init();</script>
    

</head>
<body>

<div id="wrapperus">

<div id="header_cont">
    <div id="header">
        <?php
	if(!is_front_page()){
?><a href="/"><?php
	}
?><img src="/wp-content/themes/happwell/img/logo.png" class="logo_img" /><?php
	if(!is_front_page()){
?></a><?php
	}
?>
<?php wp_nav_menu( array( 'theme_location' => 'top_static_pages' ) ); ?>


    </div>
    
    <div id="top_menu_cont">
     <div class="closerr"></div>  
     <div class="afterus"></div> 

        
        
        <?php

  $taxonomy     = 'product_cat';
  $orderby      = 'name';  
  $show_count   = 0;      // 1 for yes, 0 for no
  $pad_counts   = 0;      // 1 for yes, 0 for no
  $hierarchical = 1;      // 1 for yes, 0 for no  
  $title        = '';  
  $empty        = 0;

  $args = array(
         'taxonomy'     => $taxonomy,
         'orderby'      => $orderby,
         'show_count'   => $show_count,
         'pad_counts'   => $pad_counts,
         'hierarchical' => $hierarchical,
         'title_li'     => $title,
         'hide_empty'   => $empty
  );
 $all_categories = get_categories( $args );
 ?>
        
        <ul class="top_menu">
    <?php
 foreach ($all_categories as $cat) {
    if($cat->category_parent == 0) {
        $category_id = $cat->term_id;       
        echo '<li class="fadeInRight wow">';
        echo '<a href="'. get_term_link($cat->slug, 'product_cat') .'">'. $cat->name .'</a>';

        $args2 = array(
                'taxonomy'     => $taxonomy,
                'child_of'     => 0,
                'parent'       => $category_id,
                'orderby'      => $orderby,
                'show_count'   => $show_count,
                'pad_counts'   => $pad_counts,
                'hierarchical' => $hierarchical,
                'title_li'     => $title,
                'hide_empty'   => $empty
        );
        
        $sub_cats = get_categories( $args2 );
            if($sub_cats) { ?>
            <div class="sub_afterus"></div> 
            <ul class="top_menu sub_categorius_menu">
                <?php
                foreach($sub_cats as $sub_category) {
                    echo  '<li><a href="'. get_term_link($sub_category->slug, 'product_cat') .'">'. $sub_category->name .'</a></li>';
                }
                ?>
                <div class="sub_closerr"></div>  
                
                </ul>
                <?php
            }
    echo '</li>';
    }
}
?></ul>


        
        
        <div class="top_basket">
            <div id="search_block" class=" fadeIn wow">
            
            
             <form role="search" method="get" class="woocommerce-product-search" action="<?php echo esc_url( home_url( '/'  ) ); ?>">
	
	<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'woocommerce' ); ?>" 
    value="<?php echo get_search_query(); ?>" name="s" id="something"
    title="<?php echo esc_attr_x( 'Search for:', 'label', 'woocommerce' ); ?>" />
    
	<input type="submit" class="search_icon" value=" " />
	<input type="hidden" name="post_type" value="product" />
</form>
            
            
            </div>
            
            <div id="basket_block">
            <a href="#" class="basket_icon menu-btn  fadeIn wow"><?php
	global $woocommerce;  
    echo sprintf(_n('%d', '%d', $woocommerce->cart->cart_contents_count, 'woothemes'), 
    $woocommerce->cart->cart_contents_count); 

?></a>
<div class="cartboxus pushy pushy-right">

<h2>YOUR CART</h2>

	<?php
			do_action( 'woocommerce_review_order_before_cart_contents' );

			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
					?>
					<div class="cartus_item">
						<div class="cart_thumb" >
                        <?php	
                        $thumbnail = apply_filters( 'woocommerce_in_cart_product_thumbnail', $_product->get_image(), $values, $cart_item_key ); echo $thumbnail;
                            ?>
                        </div>
                        <div class="cart_item_content">
                            <h3><a href="<?php echo apply_filters( 'woocommerce_cart_item_name', $_product->get_permalink(), $cart_item, $cart_item_key ) . '&nbsp;'; ?>">
                            <?php echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;'; ?>
                             <?php echo WC()->cart->get_item_data( $cart_item ); ?>
                            </a></h3>
                            
                            
                            
                            <div class="cart_item_quantity">
                            
                            
                            <button class="qty__btn minus" >-</button>
                            <?php echo apply_filters( 'woocommerce_checkout_cart_item_quantity', 
                            ' <strong class="product-quantity">' . sprintf( ' %s', $cart_item['quantity'] ) . '</strong>', 
                            $cart_item, $cart_item_key ); ?>
                            
                            
                            <button class="qty__btn plus" >+</button>
                            </div>
                            
                            <div class="cart_item_price">
							<?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); ?>
						  </div>
                        
                        
                        </div>
                        
                        
                        
					</div>
					<?php
				}
			}

			do_action( 'woocommerce_review_order_after_cart_contents' );
		?>
        
        <a href="/checkout" class="gotocheckout">GO TO CHECKOUT</a>
        <div class="closer"></div>
        
        
</div>
<div class="site-overlay"></div>

<script src="/wp-content/themes/happwell/js/pushy.min.js"></script>
            </div>
        </div>
    </div>    
</div>

<?php 
if(is_front_page()){
echo do_shortcode("[huge_it_slider id='1']"); 
}


#list_hooked_functions();

?>
