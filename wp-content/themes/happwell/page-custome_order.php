<?php

/*
Template Name: Custome order.php
*/

get_header(); ?>

<?php if (have_posts()) : ?>
       <?php while (have_posts()) : the_post(); ?>
        
            <h1><span><?php wp_title(""); ?></span></h1>        
            <div class="post page row" id="post-<?php the_ID(); ?>">
            

               <?php
               echo do_shortcode('[contact-form-7 id="543" title="CREATE YOUR DESIGN"]');
                ?>
               
                 
        </div>
               <?php endwhile; ?>
            <p><?php next_posts_link('&laquo; Previous Entries') ?>
<?php previous_posts_link('Next Entries &raquo;') ?></p>
        <?php else : ?>
            <h2 class="t-center">Not Found</h2>
            <p class="t-center">Sorry, but you are looking for something that
isn't here.</p>
        <?php endif; ?>

<?php get_footer(); ?>
