<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'desolab_test');

/** MySQL database username */
define('DB_USER', 'desolab_test');

/** MySQL database password */
define('DB_PASSWORD', 'uzq4Q183');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ';#b=^fWri4gSES&^9m+&ypuX;G+fdXdP4FvADjtO5ntd>+dbvVAWvSM<#r9zGT%z');
define('SECURE_AUTH_KEY',  'iNdA+HhID->SA< ^c-IQy[kca&R~7J/8r:<U7iEk`&$KUF%Jg3N[aNxmF6^VwssZ');
define('LOGGED_IN_KEY',    'LQpp8yT.Z[YU[99OgeXWF<X;2jB(5~>]cz{v*Styz -J;f$G(.i>_&d3Y/,6p*bR');
define('NONCE_KEY',        'J]AM!}$v3k1<d}[vVZ UT/wG2bk(<+O!C4)Y]Ww}tHafQz6Dk^2QRY#!i0Eau}8V');
define('AUTH_SALT',        '?k+6lK{([ci)yX9Lz7`]BgE*7<(]{s!;EPMZG8e1]Yt:w%T!>d+Wiku@2GZUq.Dt');
define('SECURE_AUTH_SALT', 'yfZpOuT> +$|pn~oOOIc`I`4AG}zT5o<S2I=:)Sim0?dr.7O_+SkqVVNo.;CuHF1');
define('LOGGED_IN_SALT',   '>pO2;V+ar(T5!&zstUKWW5#cg4k/ Ryn)%GA=9HUv>{hrd2A]$9P3/C4L@+NdP))');
define('NONCE_SALT',       ' h =&U4>vvDlYz-C!kH<(0v>M{gbf}|5`5%lX7p-`8./8@N_`,zEy#d#v{pB%pmO');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
