<?php
/*
Template Name: Contacts
*/



get_header(); ?>

<?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>
        
            <h1><span><?php wp_title(""); ?></span></h1>        
            <div class="post page row" id="post-<?php the_ID(); ?>">
            
                <div class="col-md-2 text_contacts justify">
                    <?php the_content('Continue reading &raquo;'); ?>
         <p class="phone_block">
        <a class="social-icon" href="tel:<?php echo get_post_meta(48, 'phone_1_link', true); ?>">
        <?php echo get_post_meta(48, 'phone_1', true); ?></a>, 
        
        <a class="social-icon" href="tel:<?php echo get_post_meta(48, 'phone_2_link', true); ?>"><?php echo get_post_meta(48, 'phone_2', true); ?></a></p>
        <p class="mail_block"><a href="mailto:<?php echo get_post_meta(48, 'mail', true); ?>"><?php echo get_post_meta(48, 'mail', true); ?></a></p>
                    
                </div>
                <div class="col-md-2 right">
                   <?php 
                   echo do_shortcode('[ve_gmap map="51"]'); ?> 
                </div>
            
               
               
               <h3><span>SEND A MESSAGE</span></h3>
               <?php
               echo do_shortcode('[contact-form-7 id="53" title="SEND A MESSAGE"]');
                ?>
                 
        </div>
               <?php endwhile; ?>

        <?php else : ?>
            <h2 class="t-center">Not Found</h2>
            <p class="t-center">Sorry, but you are looking for something that
isn't here.</p>
        <?php endif; ?>
        <script type="text/javascript">
        $(document).ready(function() {
         //   $( ".footer_contacts p.phone_block, .footer_contacts p.mail_block" ).clone().appendTo( ".text_contacts" );
            });
        </script>

<?php get_footer(); ?>
