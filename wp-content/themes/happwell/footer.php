</div>
<!-- END of wrpperus -->

<div id="footer_cont">
    <div id="footer" class="row ">
    
        <div class="footer_contacts col-md-3">
        
         
        <ul class="card_holder ">
            <li title="Visa" ><img src="/wp-content/themes/happwell/img/icon_visa.png" /></li>
            <li title="MasterCard" ><img src="/wp-content/themes/happwell/img/icon_mastercard.png" /></li>            
            <li title="Discover Network"><img src="/wp-content/themes/happwell/img/icon_discover.png" /></li>
            <li title="American Express" ><img src="/wp-content/themes/happwell/img/icon_aex.png" /></li>
        </ul>
        
        
        
        
        </div>
        
         <div class="col-md-3 center">
                <a href="#header_cont" class="go_to_top">
                
                <img src="/wp-content/themes/happwell/img/logo_bottom_up.png"  />
                 HAPPWELL
                </a>
        </div>
        
        <div class="col-md-3 right middle">
        <div class="footer_socials">
        <?php 
        if (get_post_meta(48, 'twitter', true))
        {
        ?>
            <a href="<?php echo get_post_meta(48, 'twitter', true); ?>" class="button btn-twitter">
            
            </a>
            <?php  }
        if (get_post_meta(48, 'facebook', true))
        {
        ?>
            <a href="<?php echo get_post_meta(48, 'facebook', true); ?>" class="button  btn-facebook"></a>
            <?php 
            }
        if (get_post_meta(48, 'pint', true) !== '')
        {
        ?>
            <a href="<?php echo get_post_meta(48, 'pint', true); ?>" class="button btn-pint"></a>
            <?php 
            }
        if (get_post_meta(48, 'google', true))
        {
        ?>            
            <a href="<?php echo get_post_meta(48, 'google', true); ?>" class="button btn-google"></a>
            <?php 
            }
        if (get_post_meta(48, 'inst', true))
        {
        ?>            
            <a href="<?php echo get_post_meta(48, 'inst', true); ?>" class="button btn-inst"></a>
            <?php 
        }
        ?>            
        </div>
      
        </div>
        
          
        
        
    <p class="copyright">Copyrights &copy; 2016 All Rights Reserved by Happwell</p>
    
    </div>
</div>

<script type="text/javascript">

$(document).ready(function() {
    
    

// for updating cart
    $('body').on('click', '.single_add_to_cart_button.button', function(e){
        
            
       
        $.ajax({
            url: "/",
            type: "get",
            data: {          
                 more_wc_products_cart : 'true',
            },
            cache: false,
            success: function(response){
            $('.basket_icon.menu-btn').empty();
            $('.basket_icon.menu-btn').append(response);
            }
        });
       setTimeout(function(){
        $.ajax({
            url: "/",
            type: "get",
            data: {          
                 more_wc_products_cart : 'fullcart',
            },
            cache: false,
            success: function(response){
            $('.cartboxus').empty();
            $('.cartboxus').append(response);
            }
        });
        },2000);
        
     });
        // END of updating cart


// for I block
                    
     displaing_numbers = 3;  
     $('body').on('click', 'a.morenews', function(e){
        var trigger = $(this);
        e.preventDefault();      
      var  per_column_prod = 3;
      var elementCategorius = $(this).attr("data-categorius");
      
        $.ajax({
            url: "/",
            type: "get",
            data: {          
                 more_wc_products : 4,
                 per_column_prod : 3,
                 last_prod_categ : elementCategorius,
                 displaing_numbers: displaing_numbers,
            },
            cache: false,
            success: function(response){
            trigger.hide();
            trigger.parent().append(response);
            displaing_numbers = displaing_numbers + per_column_prod ;
            //alert(displaing_numbers);
            }
        });
        
     });
     
// for II block
                    

    
    
        $("a.go_to_top").click(function () {
        var elementClick = $(this).attr("href");
        var destination = $(elementClick).offset().top;
        $("html:not(:animated),body:not(:animated)").animate({scrollTop: destination}, 800);
        return false;
        });

var nav = $('#top_menu_cont');

    $(window).scroll(function () {
        if ($(this).scrollTop() > 115) {
            nav.addClass("f-nav");
        } else {
            nav.removeClass("f-nav");
        }
    });


    //search_block input
    on_search = 0;
    $('body').on('click', '.search_icon', function(e){
    //$('.search_icon, #something').click(function(e){
        
        if(on_search === 0){
        on_search = 1;
        e.preventDefault();
        $( "#something" ).animate({
            paddingLeft: "+=15",
            width: "+=130"
          }, 300, function() {});
          
        $( ".search_icon" ).animate({
            borderTopLeftRadius: "-=15",
            borderBottomLeftRadius: "-=15"
          }, 50, function() {});
        }
        else{
            on_search = 0;
            $( "#something" ).animate({
            paddingLeft: "-=15",
            width: "-=130"
          }, 50, function() {});
          
        $( ".search_icon" ).animate({
            //borderTopLeftRadius: "+=15",
            //borderBottomLeftRadius: "+=15"
          }, 300, function() {});
        }
    });
    //for wrong animation
    /*
 $('body').on('click', '*', function(e){
     var target = e.target;
    if((on_search === 1) && !$(target).is('.search_icon') && !$(target).is('#something') ){
        on_search = 0;
    $( "#something" ).animate({
            paddingLeft: "-=15",
            width: "-=130"
          }, 50, function() {});
          
        $( ".search_icon" ).animate({
            //borderTopLeftRadius: "+=15",
            //borderBottomLeftRadius: "+=15"
          }, 300, function() {});
    }
 });
 */


// cartboxus
$('.cartboxus .closer').click(function(){
    $(".site-overlay").click();
});


// for responcive design
    ///////////////////////////////////////////////
    
    
    $('.sub_afterus').click(function(a){                
                //alert('good connection');               
                $(this).next().toggleClass('sub_active');
                $(this).toggleClass('sub_active');
                return false;
            });
    
    function adaptivus() {
        var wwc = $(window).width();
        var wwh = $(window).height();
        var isMobile = window.matchMedia("only screen and (max-width: 1024px)");
        
        
                        
        
        
        if (isMobile.matches) {
        //if(wwc <= 1024){
            
            
            
            
            $( ".footer_contacts.col-md-3, .col-md-3.center" ).insertAfter( ".col-md-3.right.middle" );
            
            // to glue responcive menu
            //$( "#menu-top_static_pages" ).insertBefore( ".top_menu" );
            $( "#menu-top_static_pages" ).prependTo( "#top_menu_cont" );
            $('#menu-top_static_pages, .top_menu').addClass('responsivus');
            
            //detail image replace
            //$( ".col-md-2.part35.left" ).prependTo( "#top_menu_cont" );
            
            //disalow animation of shower
            $('.top_menu').removeClass('wow ');
            $('.top_menu li').removeClass('fadeInRight wow ');
            //$('.col-md-3.right.middle').clone().appendTo('#top_menu_cont');
            
            
            // menu display
            $('#top_menu_cont .afterus').click(function(){
                
                $('#top_menu_cont').addClass('act_menu');
                $('#top_menu_cont #menu-top_static_pages, #top_menu_cont .top_menu.responsivus').show("slow");      
                
                      
            });
            
            //menu hidder
            $('#top_menu_cont .closerr').click(function(){                
                $('#top_menu_cont #menu-top_static_pages, #top_menu_cont .top_menu.responsivus').hide("slow");
                $('#top_menu_cont').delay(500).removeClass('act_menu');     
                
                return(false);       
            });
            
            
            // logo image
            //$("img.logo_img").attr('src', '/wp-content/themes/happwell/img/logo_bottom.png');
              
            // img link deactivate
            $('.woocommerce-main-image.zoom').on('click', function(e) {
            e.preventDefault();
            //return true;
            //alert('Now  You can call');
            });
            
            
            if (wwc > wwh){} else{}
            $('.top_menu').css({ "display": ""});

        
        }
        else{
            $('.nav.menu').removeClass('responsive '); 
            // back footer elements
            $(".col-md-3.center" ).insertAfter( ".footer_contacts.col-md-3" );
            $('.col-md-3.right.middle').insertAfter( ".col-md-3.center" );
            
            
            //
            // separate responcive menu
            
            $( "#menu-top_static_pages" ).prependTo('.menu-top_static_pages-container');
            $('#menu-top_static_pages, .top_menu').removeClass('responsivus');
            $('.top_menu').addClass(' wow animated ').css({ "display": "inline-block"});
            $('.top_menu li').addClass('fadeInRight wow animated ').css({ "visibility": "visible"});
            $('#top_menu_cont .col-md-3.right.middle').clone().appendTo('');
            
             // logo image
            //$("img.logo_img").attr('src', '/wp-content/themes/happwell/img/logo.png');
            
            }
            
           
        }
// END of function

$( window ).load(function() {            
                adaptivus.call(this);
});
$(window).bind('resize', function () { 
                adaptivus.call(this);
});




});

</script>
<?php 
wp_footer(); 
?>
</body>
</html>