<?php
	###
    # WOOCOMMERCE
    ###
    
    // Display 24 products per page. Goes in functions.php
    add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 2400;' ), 20 );
    
    function list_hooked_functions($tag=false){
 global $wp_filter;
 if ($tag) {
  $hook[$tag]=$wp_filter[$tag];
  if (!is_array($hook[$tag])) {
  trigger_error("Nothing found for '$tag' hook", E_USER_WARNING);
  return;
  }
 }
 else {
  $hook=$wp_filter;
  ksort($hook);
 }
 echo '<pre>';
 foreach($hook as $tag => $priority){
  echo "<br />&gt;&gt;&gt;&gt;&gt;\t<strong>$tag</strong><br />";
  ksort($priority);
  foreach($priority as $priority => $function){
  echo $priority;
  foreach($function as $name => $properties) echo "\t$name<br />";
  }
 }
 echo '</pre>';
 return;
}
    
    # remove breadcrumbs
    add_action( 'init', 'jk_remove_wc_breadcrumbs' );
    function jk_remove_wc_breadcrumbs() {
        remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
    }
    
    # remove ordering
    remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
    
    # Remove Add to Cart Buttons from Product Page
    #remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);

    /*
    function remove_loop_button(){
    remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
    }
    add_action('init','remove_loop_button');
    */
    
    # remove bottom sidebar in the cathegory page
    remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10);
    
    /*
    @hooked woocommerce_template_single_title - 5
             * @hooked woocommerce_template_single_price - 10
             * @hooked woocommerce_template_single_excerpt - 20
             * @hooked woocommerce_template_single_add_to_cart - 30
             * @hooked woocommerce_template_single_meta - 40
             * @hooked woocommerce_template_single_sharing - 50
    */
    
    # ordering in the detail product page
    #removing
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
        remove_action('woocommerce_single_product_summary', 'woocommerce_short_description', 50);
        // Removes tabs from their original loaction 
        remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
        // Removes message of adding to cart
        


    # NEW ordering 
        
        add_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
        add_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
        add_action( 'woocommerce_single_product_summary', 'woocommerce_output_product_data_tabs', 60 ); # tabs content in 002        
        add_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 200);


        #change text buttom
        add_filter( 'woocommerce_product_single_add_to_cart_text', 'woo_custom_cart_button_text' );   
        function woo_custom_cart_button_text() { 
                return __( '+ ADD TO CART', 'woocommerce' ); 
        }

    # COMM 002
    # edition of the tabs (content)
    add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );   

    function woo_remove_product_tabs( $tabs ) {
    
        unset( $tabs['description']['title'] );      	// Remove the description tab
        unset( $tabs['reviews'] ); 			// Remove the reviews tab
        unset( $tabs['additional_information'] );  	// Remove the additional information tab
    
        return $tabs;
    
    }
    
    # related products
    function woo_related_products_limit() {
  global $product;
	
	$args['posts_per_page'] = 3;
	return $args;
    }
    add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args' );
      function jk_related_products_args( $args ) {
    	$args['posts_per_page'] = 3; // 4 related products
    	$args['columns'] = 1; // arranged in 2 columns
    	return $args;
    }
    
    #quantity
    /*
    add_action( 'wp_enqueue_scripts', 'wcqi_enqueue_polyfill' );
    function wcqi_enqueue_polyfill() {
        wp_enqueue_script( 'wcqi-number-polyfill' );
    }
    */
    
        
    # AJAX function
    
    
    #CHECKOUT
    # remove other fields
    add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

    function custom_override_checkout_fields( $fields ) {
   
    
    unset($fields['billing']['billing_company']);
    unset($fields['billing']['billing_address_2']);
    unset($fields['billing']['billing_state']);
    
    
    
    return $fields;
    }
    
    #CHECKOUT
    # set the class and placeholder of fields
    add_filter( 'woocommerce_billing_fields', 'custom_woocommerce_billing_fields' );
    function custom_woocommerce_billing_fields( $fields ) {
    
       $fields['billing_email']	= array(
          'placeholder'    => __('Email *', 'woothemes'),
          'required'       => true,
          'class'          => array('billing_email')
       );
       
       $fields['billing_phone']	= array(
          'placeholder'    => __('Phone *', 'woothemes'),
          'required'       => true,
          'class'          => array('billing_phone')
       );
       
       $fields['billing_first_name']	= array(
          'placeholder'    => __('Name *', 'woothemes'),
          'required'       => true,
          'class'          => array('billing_first_name col-md-2')
       );
       $fields['billing_last_name']	= array(
          'placeholder'    => __('Last Name *', 'woothemes'),
          'required'       => true,
          'class'          => array('billing_last_name col-md-2')
       );
       
       /*
       $fields['billing_country']	= array(
          'placeholder'    => __('Country *', 'woothemes'),
          'required'       => true,
          'class'          => array('billing_country col-md-2')
       );
       
       $fields['billing_state']	= array(
          'placeholder'    => __('State *', 'woothemes'),
          'required'       => true,
          'class'          => array('billing_state col-md-2')
       );
       */
       
       $fields['billing_postcode']	= array(
          'placeholder'    => __('Postcode *', 'woothemes'),
          'required'       => true,
          'class'          => array('billing_postcode col-md-2')
       );
       
       $fields['billing_city']	= array(
          'placeholder'    => __('City *', 'woothemes'),
          'required'       => true,
          'class'          => array('billing_city  col-md-2')
       );
    
    $fields['billing_address_1']	= array(
          'placeholder'    => __('Address *', 'woothemes'),
          'required'       => true,
          'class'          => array('billing_address_1')
       );
    
    
    
     return $fields;
    }
    
    #CHECKOUT
    # remove other fields
    add_filter("woocommerce_checkout_fields", "order_fields");
    function order_fields($fields) {
    
        $order = array(
            "billing_email",
            "billing_phone",             
            "billing_first_name", 
            "billing_last_name", 
            #"billing_country", 
            "billing_city",
            "billing_postcode",             
            "billing_address_1"
    
        );
        foreach($order as $field)
        {
            $ordered_fields[$field] = $fields["billing"][$field];
        }
    
        $fields["billing"] = $ordered_fields;
        return $fields;
    
    }
    
    
    ###
    # BASE FUNCTIONS
    ###
    
    # Menu displaing    
    function register_my_menu() {
    register_nav_menu('top_static_pages',__( 'top_static_pages' ));
    }
    add_action( 'init', 'register_my_menu' );
    
    # page displaing  
    
    ###
    # CONTACTS FORM
    ###
    
    # html5 enable    
    add_filter( 'wpcf7_support_html5_fallback', '__return_true' );
    
    ###
    #ADMIN PATH
    ###
    
    ### 
        add_action( 'woocommerce_product_data_panels', 'gowp_global_variation_price' );
        function gowp_global_variation_price() {
        	global $woocommerce;
        	?>
        		<script type="text/javascript">
        			function addVariationLinks() {
        				a = jQuery( '<a href="#">Apply to all Variations</a>' );
        				b = jQuery( 'input[name^="variable_regular_price"]' );
        				a.click( function( c ) {
        					d = jQuery( this ).parent( 'label' ).next( 'input[name^="variable_regular_price"]' ).val();
        					e = confirm( "Change the price of all variations to " + d + "?" );
        					if ( e ) b.val( d ).trigger( 'change' );
        					c.preventDefault();
        				} );
        				b.prev( 'label' ).append( " " ).append( a );
        			}
        			<?php if ( version_compare( $woocommerce->version, '2.4', '>=' ) ) : ?>
        				jQuery( document ).ready( function() {
        					jQuery( document ).ajaxComplete( function( event, request, settings ) {
        						if ( settings.data.lastIndexOf( "action=woocommerce_load_variations", 0 ) === 0 ) {
        							addVariationLinks();
        						}
        					} );
        				} );
        			<?php else: ?>
        				addVariationLinks();
        			<?php endif; ?>
        		</script>
        	<?php
        }

    


    
    
?>